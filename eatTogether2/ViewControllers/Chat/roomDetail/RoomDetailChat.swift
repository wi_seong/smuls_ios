//
//  RoomDetailChat.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/25.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import SwiftyJSON

class RoomDetailChat: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var roomTitleLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var explainTextView: UITextView!
    
    @IBOutlet weak var memberCountLabel: UILabel!
    
    @IBOutlet weak var genderFixLabel: UILabel!
    
    var roomInfo: RoomInfo?
    var MemberList = [MemberInfo]()
    var leader:JSON?
    var members:JSON?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        debugLabels()
        putDataInViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    private func debugLabels(){
        self.title = "클럽 상세보기"

        
    }
    
    private func putDataInViews(){
        roomTitleLabel.text = roomInfo!.room_name
        explainTextView.text = roomInfo!.activity_explain
        timeLabel.text = roomInfo!.appointed_at
        locationLabel.text = roomInfo!.location
        memberCountLabel.text = roomInfo!.current_people + "/" + roomInfo!.max_people + "명"
        genderFixLabel.text = "· " + roomInfo!.gender_fix
        imageView.image = roomInfo!.img
        adjustUITextViewHeight(arg: explainTextView)
        
        for i in 0 ..< members!.count{
            self.MemberList.append(MemberInfo(members![i]))
        }
    }
    
    private func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RoomDetailChat: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MemberList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(MemberList[indexPath.row].nick_name)
        
        if indexPath.row == 0{
            var cell: leaderTableViewCell
            cell = tableView.dequeueReusableCell(withIdentifier: "leaderCell") as! leaderTableViewCell
            cell.configureCell(MemberList[indexPath.row].nick_name)
            return cell
            
        } else{
            var cell: memberTableViewCell
            cell = tableView.dequeueReusableCell(withIdentifier: "memberCell") as! memberTableViewCell
            cell.configureCell(MemberList[indexPath.row].nick_name)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let storyboard = UIStoryboard (name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IntroduceController")as! IntroduceController

        // Communicate with new VC - These values are stored in the destination
        // you can set any value stored in the destination VC here
        vc.nick_name_text = MemberList[indexPath.row].nick_name
        vc.introduceMessage = MemberList[indexPath.row].introduce
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
