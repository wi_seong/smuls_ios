//
//  ChatViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/26.
//  Copyright © 2020 alomteamd. All rights reserved.
//
import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var NoRoomImage: UIImageView!
    
    @IBOutlet weak var NoRoomMsg: UILabel!
    
    var Parti = [Participating]()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = CGPoint(x: view.frame.size.width  / 2,
                                           y: view.frame.size.height / 2 - 50)
        activityIndicator.hidesWhenStopped = false
        activityIndicator.style = .large
    
        return activityIndicator
        
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        debugLabels()
        
        // 스와이프로 뒤로가기 허가
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        //indicator start
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.startAnimating()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //구분선 없애기
        tableView.separatorStyle = .none
        
        // 내가 참여한 방목록 가져오기
        getRoomHistory(){
            self.tableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
        //dummyData()
        
        // 새로고침했을 때 불러오는 함수추가
        initRefresh()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // 새로운 뉴스가 있다면 새로고침 해주기
        investigateNews()
        
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        
        // tab bar 보이기(애니메이션)
        tabBarController?.tabBar.isHidden = false
        UIView.transition(with: tabBarController!.view, duration: 0.2, options: .transitionCrossDissolve, animations: nil)
        
        
        // 네비게이션 팝제스처 추가
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // 배지 갯수 0으로 만들어주기
        UIApplication.shared.applicationIconBadgeNumber = 0
        
    }

    // MARK: - Button clicked
    
    
    // MAKR: - Funcs
    
    private func investigateNews(){
        if sessionInfo.isRequiredToRefreshParti{
            self.activityIndicator.startAnimating()
            getRoomHistory {
                self.activityIndicator.stopAnimating()
                sessionInfo.isRequiredToRefreshParti = false
            }
        }
    }
    
    func NoRoom(){
        //NoRoomImage.isHidden = false
        NoRoomMsg.isHidden = false
        //NoRoomImage.center = CGPoint(x: view.frame.size.width  / 2, y: view.frame.size.height / 2 - 50)
        //NoRoomMsg.topAnchor.constraint(equalTo: NoRoomImage.bottomAnchor, constant: 20).isActive = true
        NoRoomMsg.center = CGPoint(x: view.frame.size.width  / 2,
                                   y: view.frame.size.height / 2 - 50)
        
    }
    
    private func debugLabels(){
        
        
        // 사용자 유도 메세지 띄어주기 (10회)
        notice()
        
        
        
        // 배경색깔
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        // Navigation bar bottom 라인 없애기
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().backgroundColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        UINavigationBar.appearance().isTranslucent = false
        // Tabbar 라인 없애기
        //UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().layer.borderWidth = 0
        
        NoRoomImage.isHidden = true
        NoRoomMsg.isHidden = true
        
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
    }
    
    private func initRefresh(){
        let refresh = UIRefreshControl()
        
        refresh.addTarget(self, action: #selector(updateUI(refresh:)), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "불러오는 중..")
        
        if #available(iOS 10.0, *){
            tableView.refreshControl = refresh
        }else{
            tableView.addSubview(refresh)
        }
    }
    @objc func updateUI(refresh: UIRefreshControl){
        //tableView.beginUpdates()
        getRoomHistory(){
            print(self.Parti)
            self.tableView.reloadData()
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
           // self.tableView.endUpdates()
            refresh.endRefreshing()
            
            
        }
        
        
    }
    private func notice(){
       
        var noticeCount = UserDefaults.standard.integer(forKey: "chatNotice")
        
        if noticeCount <= 10{
            let message = "\n[메시지]에서 이야기 해보세요. 나만 아는 정보를 공유하고,\n무엇을 할지 구체적으로 정해보아요.\n\n약속시간에 맞춰 도착했다면 [출첵]에서\n 출결을 입력하고 보증금을 돌려받아요😆"
            OKDialog(message)
            noticeCount += 1
            UserDefaults.standard.set(noticeCount, forKey: "chatNotice")
        }
        
        
    }
    
    private func getRoomHistory(handler: @escaping()-> Void){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let _id = selfuser[0]._id
        
        let url = dbInfo.service_url + "api/myRoom"
        
        let body = ["_id": _id]
            
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in
            
            self.Parti.removeAll()
            
            let array = json["room_history"].array!
             
            if array.count == 0{
                
                self.NoRoom()
                handler()
                
            }
            else{
                print("룸개수: \(array.count)")
                self.debugLabels()
                
                for value in array{
                    let p = Participating(value)
                    
                    if self.isPassed(p.appointed_at){
                        p.isPassed = true
                    }
                    self.Parti.append(p)
                }
                self.Parti.reverse()
                
                handler()
            }
                
        }
    }
    
    private func isPassed(_ ap: String) -> Bool{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: ap)!
        
        let plus15 = Calendar.current.date(byAdding: .day, value: 3, to: date)!
        
        let now = Date()
        
        
        let interval:Float64 = now.timeIntervalSince(plus15)
        
        print(interval)
        if interval > 0{ return true}
        
        return false
    }

    
    private func dummyData(){
        /*
        let temp = Participating(room_id : "716a76bf9c9a456aacfb58f6b6a2c1f5", img: "test", room_name: "성북천 러닝크루 구해봅니다", activity_explain: "test", appointed_at: "2021-12-09T00:10:00.000Z", current_people: 1, gender_fix: "none", max_people: 3)
        let temp2 = Participating(room_id : "e23dfde4cf3d45cf82fd2135d87a62ec", img: "test", room_name: "C언어 공부할 사람?", activity_explain: "test", appointed_at: "2020-12-09T00:20:00.000Z", current_people: 1, gender_fix: "none", max_people: 3)
        
        Parti.append(temp)
        Parti.append(temp2)
        
        tableView.reloadData()
         */
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Parti.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if Parti[indexPath.row].isPassed{
            var cell: CompleteCell
            cell = tableView.dequeueReusableCell(withIdentifier: "complete") as! CompleteCell
            
            cell.configureCell(parti: Parti[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
            
        } else{
            var cell: JoinedCell
            cell = tableView.dequeueReusableCell(withIdentifier: "joined") as! JoinedCell
            
            cell.Chatdelegate = self
            cell.Attenddelegate = self
            cell.talkLabel.tag = indexPath.row
            cell.attendanceLabel.tag = indexPath.row
            
            cell.configureCell(parti: Parti[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let storyboard = UIStoryboard (name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RoomDetailController")as! RoomDetailController

        // Communicate with new VC - These values are stored in the destination
        // you can set any value stored in the destination VC here
        vc.flag = 1
        vc.Parti = Parti[indexPath.row]
        vc.members = Parti[indexPath.row].members
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
 
}

extension ChatViewController: CellChatDelegate, CellAttendDelegate{
    
    
    func ChatBtnTapped(tag: Int) {
        print(tag)
        
        let storyboard = UIStoryboard (name: "Chat", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomViewController")as! ChatRoomViewController

        // Communicate with new VC - These values are stored in the destination
        // you can set any value stored in the destination VC here
        //vc.RoomId = Parti[tag].room_id
        //vc.leaderUUID = Parti[tag].leader_uuid
        vc.Parti = Parti[tag]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func AttendBtnTapped(tag: Int){
        print("\(tag)출석체크버튼 tapped")
        
        let storyboard = UIStoryboard (name: "Chat", bundle: nil)
        
        if isPrevious15(Parti[tag].appointed_at){
            let vc = storyboard.instantiateViewController(withIdentifier: "AttendanceCheckController")as! AttendanceCheckController
            vc.room_id = Parti[tag].room_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let vc = storyboard.instantiateViewController(withIdentifier: "CompleteAttendController")as! CompleteAttendController
            vc.room_id = Parti[tag].room_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
 
        
    }
    
    private func isPrevious15(_ time: String) -> Bool{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: time)!
        
        let plus15 = Calendar.current.date(byAdding: .minute, value: 15, to: date)!
        
        let now = Date()
        
        
        let interval:Float64 = now.timeIntervalSince(plus15)
        
        print(interval)
        
        if interval <= 0{ return true}
        
        else {return false}
    }
    
    
}
