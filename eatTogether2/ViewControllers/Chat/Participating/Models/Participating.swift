//
//  Participating.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/14.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import Foundation
import SwiftyJSON


class Participating: CustomStringConvertible{
    
    
    var isPassed = false
    var room_id = ""
    var img: UIImage?
    var room_name = ""
    var activity_explain = ""
    var appointed_at = ""
    var current_people = ""
    var gender_fix = ""
    var location = ""
    //var how_hot = ""
    //leader
    //leader
    var max_people = ""
    
    var leader_uuid = ""
    var koreanTime = ""
    
    var members: JSON
    
    //var members: JSON
    //members
    //members
    //var reported_count = ""
    //var updated_at = ""
    /*
    init(room_id: String, img: String, room_name: String, activity_explain: String, appointed_at: String, current_people: Int, gender_fix: String, max_people: Int ){
        self.room_id = room_id
        self.img = img
        self.room_name = room_name
        self.activity_explain = activity_explain
        self.appointed_at = appointed_at
        self.current_people = String(current_people)
        self.gender_fix = gender_fix
        self.max_people = String(max_people)
        
    }*/
    

    
    init(_ data: JSON){
    
        self.room_id = data["room_id"].stringValue
        
        let t1 = data["img"].stringValue
        
        let url = URL(string: t1)

        if(url != nil){
            let data = try? Data(contentsOf: url!)
            if(data != nil){
                self.img = UIImage(data: data!)
            }else{
                print("image data is nil")
            }

        }
        self.room_name = data["room_name"].stringValue
        self.location = data["location"].stringValue
        self.activity_explain = data["activity_explain"].stringValue
        
        let t2 = data["appointed_at"].stringValue
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: t2)!
        
        let changedDate = DateFormatter()
        
        
        changedDate.locale = Locale(identifier: "ko_KR")
        changedDate.timeZone = TimeZone(abbreviation: "KST")
    
        
        
        changedDate.dateFormat = "M.d EEEE "
        let dateOutput = changedDate.string(from: date)
        changedDate.locale = Locale(identifier: "en_US")
        changedDate.dateFormat = "h:mm a"
        let time = changedDate.string(from: date)
        let kr = dateOutput + time
        let timestamp = kr
        
        
        self.koreanTime = timestamp
        self.appointed_at = data["appointed_at"].stringValue
        self.current_people = String(data["current_people"].intValue)
        self.leader_uuid = data["leader"]["uuid"].stringValue
        let add = data["gender_fix"].stringValue
        if add == "M"{
            self.gender_fix = "Men"
        }
        else if add == "F"{
            self.gender_fix = "Women"
        }
        else{
            self.gender_fix = "All"
        }
        
        self.max_people = String(data["max_people"].intValue)
        
        self.members = data["members"]


    }
    
    
    
    var description: String{
        return "roomInfo: \(self.room_id) passed: \(self.isPassed)"
    }
    
    
    
}
