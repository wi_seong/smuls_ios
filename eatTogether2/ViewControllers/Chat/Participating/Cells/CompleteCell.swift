//
//  CompleteCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/14.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class CompleteCell: UITableViewCell{
    
    var roomId = ""
    
    @IBOutlet weak var CompleteLabel: UILabel!
    
    @IBOutlet weak var background: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var placeLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var dateTimeBack: UIView!
    
    @IBOutlet weak var dateTimeSub: UIView!

    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        background.layer.borderWidth = 2
        background.layer.borderColor = UIColor.black.cgColor
        background.layer.cornerRadius = 5
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.masksToBounds = false
        background.layer.shadowOffset = CGSize(width: 1, height: 4)
        background.layer.shadowRadius = 8
        background.layer.shadowOpacity = 0.3
        
        CompleteLabel.layer.cornerRadius = 5
        CompleteLabel.layer.borderColor = UIColor.gray.cgColor
        CompleteLabel.layer.borderWidth = 1
        
        dateTimeBack.layer.borderColor = UIColor.black.cgColor
        dateTimeBack.layer.borderWidth = 1
        
        dateTimeSub.layer.borderColor = UIColor.black.cgColor
        dateTimeSub.layer.borderWidth = 1
        
        dateLabel.adjustsFontSizeToFitWidth = true
        timeLabel.adjustsFontSizeToFitWidth = true
        placeLabel.adjustsFontSizeToFitWidth = true
        titleLabel.adjustsFontSizeToFitWidth = true
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    public func configureCell(parti: Participating){
        self.roomId = parti.room_id
        self.titleLabel.text = parti.room_name
        self.placeLabel.text = parti.location
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: parti.appointed_at)!
        
        //print(parti.appointed_at)
        
        let changedDate = DateFormatter()
        
        
        changedDate.locale = Locale(identifier: "ko_KR")
        changedDate.timeZone = TimeZone(abbreviation: "KST")
    
        
        changedDate.dateFormat = "MM.dd"
        self.dateLabel.text = changedDate.string(from: date)
        changedDate.dateFormat = "a"
        let one = changedDate.string(from: date)
        changedDate.dateFormat = "h:mm"
        
        let two = changedDate.string(from: date)
        let sum = one + "\n" + two
        self.timeLabel.text = sum
        /*
        changedDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let plus15 = Calendar.current.date(byAdding: .minute, value: 15, to: date)!
        
        let now = Date()
        
        let interval = now.timeIntervalSince(plus15)
        
        if interval > 0{ passedRoomSet() }
        
        print("\(parti.room_name): \(interval)")
        
        */

        
        
        
        
    }
    

}
