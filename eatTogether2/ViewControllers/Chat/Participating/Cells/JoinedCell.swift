//
//  JoinedCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/14.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

protocol CellAttendDelegate: class{
    func AttendBtnTapped(tag: Int)
}
protocol CellChatDelegate: class{
    func ChatBtnTapped(tag: Int)
    
}
class JoinedCell: UITableViewCell{
    
    var roomId = ""
    
    weak var Chatdelegate: CellChatDelegate?
    weak var Attenddelegate: CellAttendDelegate?
    
    @IBOutlet weak var background: UIView!
    
    @IBOutlet weak var JoinedLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var placeLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var attendanceLabel: UIButton!
    
    @IBOutlet weak var talkLabel: UIButton!
    
    @IBOutlet weak var dateTimeBack: UIView!
    
    @IBOutlet weak var dateTimeSub: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        background.layer.borderWidth = 2
        background.layer.borderColor = UIColor.black.cgColor
        background.layer.cornerRadius = 5
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.masksToBounds = false
        background.layer.shadowOffset = CGSize(width: 1, height: 4)
        background.layer.shadowRadius = 8
        background.layer.shadowOpacity = 0.3
        
        JoinedLabel.layer.cornerRadius = 5
        JoinedLabel.layer.borderColor = UIColor.gray.cgColor
        JoinedLabel.layer.borderWidth = 1
        
        dateTimeBack.layer.borderColor = UIColor.black.cgColor
        dateTimeBack.layer.borderWidth = 1
        
        dateTimeSub.layer.borderColor = UIColor.black.cgColor
        dateTimeSub.layer.borderWidth = 1
        
        talkLabel.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        talkLabel.layer.cornerRadius = 23
        talkLabel.layer.borderWidth = 2
        talkLabel.layer.borderColor = UIColor.black.cgColor
        talkLabel.tintColor = UIColor.black
        talkLabel.layer.shadowColor = UIColor.black.cgColor
        talkLabel.layer.masksToBounds = false
        talkLabel.layer.shadowOffset = CGSize(width: 1, height: 3)
        talkLabel.layer.shadowRadius = 8
        talkLabel.layer.shadowOpacity = 0.2
        
        dateLabel.adjustsFontSizeToFitWidth = true
        timeLabel.adjustsFontSizeToFitWidth = true
        placeLabel.adjustsFontSizeToFitWidth = true
        titleLabel.adjustsFontSizeToFitWidth = true
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    public func configureCell(parti: Participating){
        self.roomId = parti.room_id
        self.titleLabel.text = parti.room_name
        self.placeLabel.text = parti.location
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: parti.appointed_at)!
        
        //print(parti.appointed_at)
        
        let changedDate = DateFormatter()
        
        
        changedDate.locale = Locale(identifier: "ko_KR")
        changedDate.timeZone = TimeZone(abbreviation: "KST")
    
        
        changedDate.dateFormat = "MM.dd"
        self.dateLabel.text = changedDate.string(from: date)
        changedDate.dateFormat = "a"
        let one = changedDate.string(from: date)
        changedDate.dateFormat = "h:mm"
        
        let two = changedDate.string(from: date)
        let sum = one + "\n" + two
        self.timeLabel.text = sum
        /*
        changedDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let plus15 = Calendar.current.date(byAdding: .minute, value: 15, to: date)!
        
        let now = Date()
        
        let interval = now.timeIntervalSince(plus15)
        
        if interval > 0{ passedRoomSet() }
        
        print("\(parti.room_name): \(interval)")
        
        */

        
        
        
        
    }
    
    
    
    @IBAction func attendanceButton(_ sender: Any) {
        print("출첵클릭")
        Attenddelegate?.AttendBtnTapped(tag: (sender as AnyObject).tag)
        
        
    }
    
    @IBAction func chatButton(_ sender: Any) {
        print("채팅버튼클릭")
        Chatdelegate?.ChatBtnTapped(tag: (sender as AnyObject).tag)
        
    }
}
