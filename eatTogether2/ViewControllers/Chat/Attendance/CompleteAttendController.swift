//
//  CompleteAttendController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/30.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class CompleteAttendController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var disagreeLabel: UIButton!
    
    var room_id: String?
    
    var states = Array<Int>()
    var names = Array<String>()
    
    var myState = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        debugLabels()
        
        getStates()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    // MARK: - Buttons
    
    @IBAction func disagreeAction(_ sender: Any) {
        if let url = URL(string: "http://pf.kakao.com/_LVjnK/chat") {
                                                UIApplication.shared.open(url, options: [:])
        }
    }
    

    
    private func debugLabels(){
        let mas = NSMutableAttributedString(string: "이의를 제기하고 싶어요", attributes: nil)
            mas.addAttributes([
                .strokeColor: UIColor.lightGray,
                .strokeWidth: -2,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ], range: NSMakeRange(0, mas.length))
        disagreeLabel.setAttributedTitle(mas, for: .normal)
        
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        //뒤로가기 버튼
        
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    private func getStates(){
        
        states.removeAll()
        names.removeAll()

        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let myId = selfuser[0]._id
        var myNick = ""
        
        
        let url = "api/check"
        let body: Parameters = [
            "room_id": room_id!
        ]
        
        let state = getMemberState(url: url, body: body)
        state.getMembersInfo(){
            (json) in
            let result = json["result"].intValue
            switch result{
            case 200:
                let members = json["message"]["members"]
                //print("멤버: \(members)")
                for idx in 0..<members.count{
                    var state = members[idx]["state"].intValue
                    let nickName = members[idx]["member"]["nick_name"].stringValue
                    let _id = members[idx]["member"]["_id"].stringValue
                    
                    if state == 0 {state = 3} // 시간이 지난 방에 대해서 (미입력)0인 값들은 3으로 고쳐준다.
                    
                    //내 아이디라면 스테이트 저장해놓기
                    if _id == myId {
                        self.myState = state
                        myNick = nickName
                    }
                    else{
                        self.states.append(state)
                        self.names.append(nickName)
                    }
                }
                // 맨마지막에 넣는 셀은 자기 자신에 대한 정보
                self.states.append(self.myState)
                self.names.append(myNick)
                print(self.names)
                self.tableView.reloadData()
                
                break
            case 400:
                self.OKDialog("멤버정보 불러오기를 실패했습니다")
                break
            default:
                break
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CompleteAttendController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == names.count - 1{
            var cell: memberAttendCell
            cell = tableView.dequeueReusableCell(withIdentifier: "MyAttendCell") as! memberAttendCell
            cell.configureCell(names[indexPath.row], states[indexPath.row])
            return cell
            
        } else{
            var cell: memberAttendCell
            cell = tableView.dequeueReusableCell(withIdentifier: "memberAttendCell") as! memberAttendCell
            cell.configureCell(names[indexPath.row], states[indexPath.row])
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: false)
        
    }
}
