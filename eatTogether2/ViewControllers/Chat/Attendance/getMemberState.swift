//
//  getMemberState.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/30.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class getMemberState: CustomStringConvertible{
    
    
    
    
    var url: String
    var body: Parameters
    
    init(url: String, body: Parameters) {
        self.url = dbInfo.service_url + url
        self.body = body
    }
    
    public func getMembersInfo(handler: @escaping (_ json: JSON) -> Void){
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in
            
            
            handler(json)
        }
    }
    
    
    
    
    var description: String{
        return "url: \(self.url)"
    }
    
}
