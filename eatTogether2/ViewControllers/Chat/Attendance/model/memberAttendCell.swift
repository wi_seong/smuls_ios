//
//  memberAttendCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/30.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class memberAttendCell: UITableViewCell{
    
    
    @IBOutlet weak var nickNameLabel: UILabel!
    
    @IBOutlet weak var stateLabel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*
        stateLabel.layer.cornerRadius = 17
        stateLabel.layer.borderWidth = 0
        stateLabel.layer.borderColor = UIColor.white.cgColor
        
        stateLabel.layer.shadowColor = UIColor.black.cgColor
        stateLabel.layer.masksToBounds = false
        stateLabel.layer.shadowOffset = CGSize(width: 0, height: 3)
        stateLabel.layer.shadowRadius = 5
        stateLabel.layer.shadowOpacity = 0.2
        */
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    public func configureCell(_ nick: String, _ state: Int){
        self.nickNameLabel.text = nick
        
        if state == 0{
            self.stateLabel.isHidden = true
        } else if state == 1{
            //self.stateLabel.setTitle("출석", for: .normal)
            self.stateLabel.setImage(UIImage(named: "state_attend"), for: .normal)
            self.stateLabel.isHidden = false
        } else if state == 2{
            self.stateLabel.setImage(UIImage(named: "state_late"), for: .normal)
            self.stateLabel.isHidden = false
        } else if state == 3{
            self.stateLabel.setImage(UIImage(named: "state_absent"), for: .normal)
            self.stateLabel.isHidden = false
        } else{
            self.stateLabel.isHidden = true
        }
        
    }
    

    
}
