//
//  AttendanceCheckController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/30.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class AttendanceCheckController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var middleExpLabel: UILabel!
    
    @IBOutlet weak var AttendBtn: UIButton!

    @IBOutlet weak var LateBtn: UIButton!
    
    @IBOutlet weak var SubmitBtn: UIButton!
    
    var room_id: String?
    var myState = 0
    
    var states = Array<Int>()
    var names = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
        
        debugLabels()
        
        getStates()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //self.tabBarController?.tabBar.isHidden = true
    }
    
    
    private func debugLabels(){
        
        // 사용자 유도 메세지
        notice()
        
        
        middleExpLabel.text = "약속시간 ±15분 안에 제출해요.\n미제출은 결석이에요😭"
        middleExpLabel.layer.cornerRadius = 25
        middleExpLabel.layer.borderWidth = 1
        middleExpLabel.layer.borderColor = UIColor.lightGray.cgColor
        
        //AttendBtn.layer.cornerRadius = 25
        //AttendBtn.layer.borderColor = UIColor.black.cgColor
        //AttendBtn.layer.borderWidth = 2
        //AttendBtn.backgroundColor = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
        
        //LateBtn.layer.cornerRadius = 25
        //ateBtn.layer.borderColor = UIColor.black.cgColor
        //LateBtn.layer.borderWidth = 2
        //LateBtn.backgroundColor = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
        
        
        SubmitBtn.layer.cornerRadius = 25
        SubmitBtn.layer.borderColor = UIColor.black.cgColor
        SubmitBtn.layer.borderWidth = 2
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        // 뒤로가기 버튼
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func notice(){
        var noticeCount = UserDefaults.standard.integer(forKey: "attendNotice")
        
        if noticeCount <= 10{
            let message = "\n스물스의 출첵법🙋🏻‍♀️🙋🏻‍♂️\n\n1. 자기 출석은 자기가 체크해요!\n2. 다른 참여자가 체크한 출결을 실시간으로\n볼 수 있어요. 실제와 다르다면 이의를\n제기해주세요.\n스물스 \'카카오톡 채널\'에서 신속하게 도와드릴게요 😁"
            OKDialog(message)
            noticeCount += 1
            UserDefaults.standard.set(noticeCount, forKey: "attendNotice")
        }
        
    }
    
    private func getStates(){
        
        states.removeAll()
        names.removeAll()
        
        //맨 위에 공집합 넣어주기
        states.append(0)
        names.append("")
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let myId = selfuser[0]._id
        var myNick = ""
        
        
        let url = "api/check"
        let body: Parameters = [
            "room_id": room_id!
        ]
        
        let state = getMemberState(url: url, body: body)
        state.getMembersInfo(){
            (json) in
            let result = json["result"].intValue
            switch result{
            case 200:
                let members = json["message"]["members"]
                //print("멤버: \(members)")
                
                print("멤버출석: \(members)")
                for idx in 0..<members.count{
                    let state = members[idx]["state"].intValue
                    let nickName = members[idx]["member"]["nick_name"].stringValue
                    let _id = members[idx]["member"]["_id"].stringValue
                    
                    
                    //내 아이디라면 스테이트 저장해놓기
                    if _id == myId {
                        self.myState = state
                        myNick = nickName
                    }
                    else{
                        self.states.append(state)
                        self.names.append(nickName)
                    }
                }
                // 맨마지막에 넣는 셀은 자기 자신에 대한 정보
                self.states.append(self.myState)
                self.names.append(myNick)
                print(self.states)
                self.tableView.reloadData()
                self.decideMyState() // 나의 출석현황 결정
                break
            case 400:
                self.OKDialog("멤버정보 불러오기를 실패했습니다")
                break
            default:
                break
            }
        }
    }
    
    private func decideMyState(){
        
        if myState == 1{
            AttendBtn.setImage(UIImage(named: "Attended"), for: .normal)
            
        }else if myState == 2{
            LateBtn.setImage(UIImage(named: "late"), for: .normal)
        }
        
    }
    private func setDefaultColorButton(){
        AttendBtn.setImage(UIImage(named: "Not_attend"), for: .normal)
        LateBtn.setImage(UIImage(named: "Not_late"), for: .normal)
    }
    
    private func submit(){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let myId = selfuser[0]._id
        
        let body: Parameters = [
            "room_id": room_id!,
            "_id": myId,
            "state": myState
        ]

        let url = dbInfo.service_url + "api/state"
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            print(result)
            switch result{
            case 200:
                self.OKDialog("체크되었어요")
                self.getStates() // 한번 더 업데이트 해주기
                break
            case 300:
                self.OKDialog("약속시간 15분전부터 15분후까지\n 체크 가능합니다.")
                self.setDefaultColorButton()
                break
            case 400:
                self.OKDialog("제출 에러 ERR 400")
                break
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
        }
    }
    
    
    @IBAction func AttendAction(_ sender: Any) {
        
        if myState == 1{
            myState = 0
            setDefaultColorButton()
        
        } else{
            myState = 1
            setDefaultColorButton()
            AttendBtn.setImage(UIImage(named: "Attended"), for: .normal)
        }
    }
    
    @IBAction func LateAction(_ sender: Any) {
        if myState == 2{
            myState = 0
            setDefaultColorButton()
        } else {
            myState = 2
            setDefaultColorButton()
            LateBtn.setImage(UIImage(named: "late"), for: .normal)
        }
    }

    @IBAction func submitAction(_ sender: Any) {
        submit()
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AttendanceCheckController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == states.count - 1{
            var cell: memberAttendCell
            cell = tableView.dequeueReusableCell(withIdentifier: "MyAttendCell") as! memberAttendCell
            cell.configureCell(names[indexPath.row], states[indexPath.row])
            return cell
            
        } else{
            var cell: memberAttendCell
            cell = tableView.dequeueReusableCell(withIdentifier: "memberAttendCell") as! memberAttendCell
            cell.configureCell(names[indexPath.row], states[indexPath.row])
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: false)
        
    }

}
