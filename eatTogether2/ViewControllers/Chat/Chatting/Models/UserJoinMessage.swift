//
//  UserJoinMessage.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/29.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation

class JoinMessage: CustomStringConvertible {
    var message: String
    
    init(message: String) {

        self.message = message

    }
    
    var description: String {
        return "[message: \(message)]"
    }
}
