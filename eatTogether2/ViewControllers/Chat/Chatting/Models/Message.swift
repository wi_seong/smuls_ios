//
//  Message.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation

class Message: CustomStringConvertible {
    var type: Int
    var user: User
    var message: String
    var timestamp: String
    
    init(type: Int, user: User, message: String, timestamp: String) {
        self.type = type
        self.user = user
        self.message = message
        self.timestamp = timestamp
    }
    
    var description: String {
        return "[user: \(user), message: \(message), timestamp: \(timestamp)]"
    }
}
