//
//  User.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation

class User: CustomStringConvertible {
    var sessionId: String
    var uuid: String
    var nickname: String
    
    init(sessionId: String, uuid: String, nickname: String) {
        self.sessionId = sessionId
        self.uuid = uuid
        self.nickname = nickname
    }
    
    var description: String {
        return "[sessionId: \(sessionId), uuid: \(uuid), username: \(nickname)]"
    }
}
