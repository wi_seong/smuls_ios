//
//  ChatRoomViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import SocketIO
import RealmSwift
import Alamofire
import SwiftyJSON

class ChatRoomViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var inputContainerBottomContraint: NSLayoutConstraint!
    
    @IBOutlet weak var sendButton: UIButton!
    
    var socketIOManager = SocketIOManager()
    
    var messages = [Message]()
    var user: User!
    var user_id = ""
    
    var nowCount = 0
    var howCount = 0
    var totalCount = 0
    
    var Parti: Participating?
    var leaderUUID = ""
    var RoomId = "716a76bf9c9a456aacfb58f6b6a2c1f5"
    
    var isLoadingMore = 1
    var loadingFirst = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        debugLabels()
        
        // delegate 위임
        messageTextField.delegate = self
        // 꾹 누르는 제스처 인지
        //let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCalled(_:)))
        
        //tableView.addGestureRecognizer(longPressGesture)
        

   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        // tab bar 숨기기
        self.tabBarController?.tabBar.isHidden = true
        //setTabBar(hidden: true)
        
        
        notificationCheck()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        initialize()
        
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

        
 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        messages.removeAll()
        //socket 커넥션 끊기
        disconnectSession()
        
        // 유저 없애주기
        unregisterUser()
        
        // chatting_room_id 초기화
        sessionInfo.chatting_room_id = ""
    }
    func debugLabels(){
        //Labels layout setting
       messageTextField.layer.cornerRadius = 16
       messageTextField.setLeftPaddingPoints(20)
       messageTextField.setRightPaddingPoints(20)
        
        
       // 처음에는 아무것도 입력을 안한상태이기 때문에 보내는 버튼 비활성화/ 글을 입력시 활성화
       sendButton.isEnabled = false
       
       tableView.delegate = self
       tableView.dataSource = self
       
       messageTextField.delegate = self
        
       NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardOpen), name: UIResponder.keyboardWillHideNotification, object: nil)
        
       let tapGuesterHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
       let tapGuesterShowKeyboard = UITapGestureRecognizer(target: self, action: #selector(showKeyboard))
       tableView.addGestureRecognizer(tapGuesterHideKeyboard)
       messageTextField.superview?.addGestureRecognizer(tapGuesterShowKeyboard)
        
        /*
        // 네비게이션 바 투명화
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        */
        // 배경색깔
        tableView.backgroundColor = UIColor(displayP3Red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
        
        
        
        // 뒤로 가기 버튼
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        

    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func notificationCheck(){
        sessionInfo.chatting_room_id = Parti!.room_id
    }
    
    
    func initialize(){
        
        
        
        
        
        // 사용자 정보 등록하기
        registerUser()
        
        if loadingFirst{ // 최초일 때만 실행하기
            // 이전 채팅 내역 불러오기
            loadChatHistory()
        }
  
        //socket Chatting 셋팅
        configureSocketChat()
    }
    
    func configureSocketChat(){


        //세션 연결하기

        socketIOManager.connect()
        
        
        socketIOManager.handleNewMessage { (message) in
            self.messages.append(message)
            self.tableView.reloadData()
            self.scrollToBottomOfChat()
        }
        
        socketIOManager.newUserConnect{ (message) in
            
            self.messages.append(message)
            self.tableView.reloadData()
            self.scrollToBottomOfChat()
        }
        
        socketIOManager.userLeftMessage{ (message) in
        
            self.messages.append(message)
            self.tableView.reloadData()
            self.scrollToBottomOfChat()
        }

        //커넥션 확인하고 연결되었으면 userJoin emit하기
        socketIOManager.didConnect{
            let msg = Message(type: 3, user: self.user, message: "n", timestamp: "none")
            //SocketIOManager.shared.userJoinOnConnect(message: msg)
            self.socketIOManager.userJoinRoom(message: msg)
            
        }

        
        
    }
    
    func disconnectSession(){
        let msg = Message(type: 3, user: self.user, message: "n", timestamp: "none")
        //SocketIOManager.shared.userJoinOnConnect(message: msg)
        self.socketIOManager.userLeaveRoom(message: msg)
        self.socketIOManager.disconnect()
        
    }
    

    @objc func reLogin() {
        //UserDefaults.standard.removeObject(forKey: "nickname") 대체할 거 갗ㅈ기
        //SocketIOManager.shared.disconnect()
        //SocketIOManager.shared.connect()
    }
    
    func scrollToBottomOfChat(){
        
        if messages.count < 3{return}
        let indexPath = IndexPath(row: messages.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
    // 로컬 DB에서 가져오기
    func loadChatHistory() {
        
        // indicator 시작
        activityIndicator.startAnimating()
        
        messages.removeAll()
        
        getTotalCount(){
            self.getMessages()
            
        }

        
    }
    
    func getTotalCount(handler: @escaping () -> Void){
        
        let body: Parameters = [
            "room_id": user.sessionId,
            "_id": user_id
        ]

        let url = dbInfo.chat_url + "roomChat"
        Methods.shared.ChatAlamoPost( url: url, body: body){ (json) in
            let result = json["result"].intValue
            
            //indicator 잠시 끄기
            self.activityIndicator.stopAnimating()
            
            switch result{
            case 200:
                self.totalCount = json["msgLen"].intValue
                print("total: \(self.totalCount)")
                
                // 기본 값 셋팅
                if self.totalCount < 20{
                    self.nowCount = self.totalCount
                    self.howCount = self.totalCount
                }
                else if self.totalCount >= 20{
                    self.nowCount = 20
                    self.howCount = 20
                }
                else if self.totalCount == 0{
                    self.nowCount = -1
                }
                
                handler()
                break
            case 401:
                self.OKDialog("본인이 속한 방이 아닙니다 401")
                break
            case 404:
                self.OKDialog("이전의 채팅내역 불러오기 실패 404")
                break
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
        }
    }
    func getMessages(){
        
        
        // 메세지 개수에 따른 메세지 불러오기 구현
        
        if nowCount < 0 || totalCount == 0{
            return
        }
        
        
        
        //indicator 켜기
        activityIndicator.startAnimating()
        
        
        
        let body: Parameters = [
            "room_id": user.sessionId,
            "now_count": nowCount,
            "how_count": howCount,
            "total_count": totalCount
        ]

        let url = dbInfo.chat_url + "chatLoad"
        Methods.shared.ChatAlamoPost( url: url, body: body){ (json) in
            
            //indicator 끄기
            
            var newSize = 0
            self.activityIndicator.stopAnimating()
            if !json["message"].exists(){
                self.OKDialog("이전의 채팅내역 불러오기 실패 502")
                return
            }
            
            
            var message = json["message"].array
            message?.reverse()
            
            newSize = message!.count
            
            
            for m in message!{
                let user = User(sessionId: self.RoomId, uuid: m["user"]["uuid"].stringValue, nickname: m["user"]["nickname"].stringValue)
                let mes = Message(type: m["type"].intValue, user: user, message: m["message"].stringValue, timestamp: m["timestamp"].stringValue)
                
                self.messages.insert(mes, at: 0) // 0번째 인덱스는 공지사항임
            }
            
            self.tableView.reloadData() // reload
            
            if self.loadingFirst{
                self.scrollToBottomOfChat()
                self.loadingFirst = false
            }
            else{
                let indexPath = IndexPath(row: newSize, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
            }
            
            if self.nowCount == self.totalCount{
                self.nowCount = -1
                //공지사항 출력하기
                self.presentNoticeMessage()
            }
            
            else if self.totalCount - self.nowCount < 20{
                self.howCount = self.totalCount - self.nowCount
                
                self.nowCount = self.totalCount
                
            }
            else {self.nowCount += self.howCount}
            
            self.isLoadingMore = 0
            
            
            
            
        }
        
    }

    
    func registerUser() {
        //if let username = UserDefaults.standard.string(forKey: "username") {
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        RoomId = Parti!.room_id
        leaderUUID = Parti!.leader_uuid
        
        self.title = Parti!.room_name
        
        
        if self.user == nil {
            let nickname: String = selfuser[0].nick_name
            let uuid: String = selfuser[0].uuid
            user_id = selfuser[0]._id
            
            self.user = User(sessionId: RoomId, uuid: uuid, nickname: nickname)

                
            //SocketIOManager.shared.userJoinOnConnect(user: user)
                
                
            // 로컬 DB에서 정보가져오기!!!
                
            /*****************************/
            //self.loadChatHistory()
        }
        
        
        
    }
    func unregisterUser(){
        let msg = Message(type: 4, user: user, message: "n", timestamp: "none")
        
        //socketIOManager.userleftDisconnect(message: msg)
        socketIOManager.disconnect()
        
        print("Socket Session unregistered")
    }
    
    
    // Send Message
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage()
        return true
    }
    
    @IBAction func sendButtonDidTap(_ sender: UIButton) {
        sendMessage()
    }
    
    func sendMessage() {
        if let message = messageTextField.text {
            if message.trimmingCharacters(in: .whitespaces) != "" {
                
                let now = Date()
                let date = DateFormatter()
                date.locale = Locale(identifier: "ko_KR")
                date.timeZone = TimeZone(abbreviation: "KST")
                
                date.dateFormat = "a h:mm"
                let kr = date.string(from: now)
                let timestamp = kr
                //user.sessionId = "test"
                
                let msg = Message(type: 2, user: user, message: message.trimmingCharacters(in: .whitespaces), timestamp: timestamp)
                socketIOManager.sendMessage(message: msg)
                messageTextField.text = ""
                sendButton.isEnabled = false
                messages.append(msg)
                tableView.reloadData()
                tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                scrollToBottomOfChat()
            }
        }
    }
    // End Send Message
    
    
    // Keyboard handler
    @objc func hideKeyboard() {
        messageTextField.resignFirstResponder()
    }
    
    @objc func showKeyboard() {
        messageTextField.becomeFirstResponder()
    }
    
    @objc func handleKeyboardOpen(notification: Notification) {
        if let userInfo = notification.userInfo {
            if messageTextField.isEditing {

                let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
                if notification.name == UIResponder.keyboardWillShowNotification {
                    inputContainerBottomContraint.constant = -keyboardFrame.height + 30

                    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height - 30, right: 0)
                    scrollToBottomOfChat()
                    //tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    
                } else {
                    inputContainerBottomContraint.constant = 0
                    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    scrollToBottomOfChat()
                    
                }
                
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    // End Keyboard handler
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messageTextField.resignFirstResponder()
        
    }
    
    
    @objc func longPressCalled(_ longPress: UILongPressGestureRecognizer){
        let locationInView = longPress.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: locationInView)
        
        
        if messages[indexPath!.row].user.uuid == user.uuid {return} // 내가 보낸 것은 무시한다.
        
        DispatchQueue.main.async {
            let message = NSLocalizedString("\"\(self.messages[indexPath!.row].message)\"\n이 메세지가 운영 정책을 위반하나요?",
                                            comment: "Customized alert")
            let actions = [
                UIAlertAction(title: NSLocalizedString("네, 위반해요", comment: "Alert button to open Settings"),
                              style: .destructive,
                              handler: nil),
                UIAlertAction(title: NSLocalizedString("취소", comment: "Alert OK button"),
                              style: .cancel,
                              handler: nil)
            ]
            
            self.alert(title: "", message: message, actions: actions)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if messages[indexPath.row].type == 1{
            var cell: NoticeTableViewCell
            
            cell = tableView.dequeueReusableCell(withIdentifier: "noticeCell") as! NoticeTableViewCell
            cell.configureNoticeMessage(message: noticeMessageContent())
            return cell
            
        }
        else if messages[indexPath.row].type == 2{
            var cell: MessageTableViewCell
            if messages[indexPath.row].user.uuid == user.uuid {
                var cell: MyMsgTableViewCell
                cell = tableView.dequeueReusableCell(withIdentifier: "outgoingCell") as! MyMsgTableViewCell
                cell.configureCell(message: messages[indexPath.row])
                return cell
            } else if messages[indexPath.row].user.uuid == leaderUUID{
                
                cell = tableView.dequeueReusableCell(withIdentifier: "leaderIncomingCell") as! MessageTableViewCell
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "incomingCell") as! MessageTableViewCell
            }
            cell.configureCell(message: messages[indexPath.row])
            return cell
        }
        else if messages[indexPath.row].type == 3{
            var cell: JoinMessageTableViewCell
            let nickname = messages[indexPath.row].user.nickname
            let message = nickname + "님이 들어왔습니다."
            let joinMessage = JoinMessage(message: message)
            cell = tableView.dequeueReusableCell(withIdentifier: "userJoinCell") as! JoinMessageTableViewCell
            cell.configureJoinMessage(message: joinMessage)
            return cell
            
        }
        else {
            var cell: LeftMessageTableViewCell
            let nickname = messages[indexPath.row].user.nickname
            let message = nickname + "님이 나갔습니다."
            let leftMessage = LeftMessage(message: message)
            cell = tableView.dequeueReusableCell(withIdentifier: "userLeftCell") as! LeftMessageTableViewCell
            cell.configureLeftMessage(message: leftMessage)
            return cell
            
        }
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView{
            
            if tableView.contentOffset.y < 10.0 && isLoadingMore == 0{
                isLoadingMore = 1
                let oldContentSizeHeight = tableView.contentSize.height
                getMessages()
                let newContentSizeHeight = tableView.contentSize.height
                tableView.contentOffset = CGPoint(x:tableView.contentOffset.x, y: newContentSizeHeight - oldContentSizeHeight)
                
            }
        }
    }
    
    
    
    func noticeMessageContent() -> NoticeMessage{
        
        let content = "설레는 마음으로 어디서 만날지\n무엇을 할지 정해보세요 ☺️\n\n운영정책을 위반한 메시지는\n\'스물스 카카오톡 채널\'로 신고 가능하며,\n스물스 이용이 제한될 수 있습니다."
        return NoticeMessage(message: content)
        
        
    }
    
    func presentNoticeMessage(){
        let message = "notice"

        let now = Date()
        let date = DateFormatter()
        date.locale = Locale(identifier: "ko_kr")
        date.timeZone = TimeZone(abbreviation: "KST")
                
        date.dateFormat = "h:mm a"
        let kr = date.string(from: now)
        let timestamp = kr
                
        let msg = Message(type: 1, user: user, message: message, timestamp: timestamp)
        
        
        messages.insert(msg, at: 0)
        tableView.reloadData()
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
        
            //if(count <= 15){
                //titleCountLabel.text = String(count)+"/15"
                
            //}
            
        if count == 0{
            self.sendButton.isEnabled = false
        } else{
            self.sendButton.isEnabled = true
        }
            return count <= 300 // 300자로 끊음
    }
    
    
    
}
