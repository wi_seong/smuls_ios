//
//  NoticeTableViewCell.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/29.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class NoticeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var noticeView: UIView!
    @IBOutlet weak var reportButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        noticeView.layer.cornerRadius = 16
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        
        // 문의 및 신고
        let mas = NSMutableAttributedString(string: "문의 및 신고", attributes: nil)
            mas.addAttributes([
                .strokeColor: UIColor.lightGray,
                .strokeWidth: -2,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ], range: NSMakeRange(0, mas.length))
        reportButton.setAttributedTitle(mas, for: .normal)
    }
    
    func configureNoticeMessage(message: NoticeMessage){
        
        noticeLabel.text = message.message
        
    }
    
    @IBAction func reportAction(_ sender: Any) {
        if let url = URL(string: "http://pf.kakao.com/_LVjnK/chat") {
                                                UIApplication.shared.open(url, options: [:])
        }
    }
    
}
