//
//  JoinMessageTableViewCell.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/29.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class JoinMessageTableViewCell: UITableViewCell {

    
   
    @IBOutlet weak var userJoinLabel: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

 
    func configureJoinMessage(message: JoinMessage){
        userJoinLabel.text = message.message
        
    }


}
