//
//  MyMsgTableViewCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/15.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class MyMsgTableViewCell: UITableViewCell {

    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(message: Message) {
        messageLabel.text = message.message
        timestamp.text = message.timestamp
        
        
        
    }

}
