//
//  MessageTableViewCell.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(message: Message) {
        usernameLabel.text = message.user.nickname
        messageLabel.text = message.message
        timestamp.text = message.timestamp
        
        
        
    }

}


