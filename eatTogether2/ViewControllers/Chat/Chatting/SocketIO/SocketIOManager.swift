//
//  SocketIOManager.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    
    //static let shared = SocketIOManager()
    
    var manager = SocketManager(socketURL: URL(string: dbInfo.chatting_url)!, config: [.log(true), .compress])

    public var socket: SocketIOClient!
    
    override init(){
        
        super.init()
        
        socket = self.manager.socket(forNamespace: "/appointment_chat")
    }
    deinit{
        socket = nil
    }

    
    func connect() {
        socket.connect()

    }
    
    func disconnect() {
        socket.disconnect()
        print("Socket Session all disconnected")
    }
    
    func userJoinRoom(message: Message){
        let msg: [String: Any] = [
            "timestamp": "none",
            "user": [
                "sessionId": message.user.sessionId
            ]
        ]
        socket.emit("userConnect", with: [msg])
    }
    
    func userLeaveRoom(message: Message){
        let msg: [String: Any] = [
            "timestamp": "none",
            "user": [
                "sessionId": message.user.sessionId
            ]
        ]
        socket.emit("userDisconnect", with: [msg])
    }
    
    func userJoinOnConnect(message: Message) {
    let msg: [String: Any] = [
                                  "type": 3,
                                  "timestamp": "none",
                                  "user": ["sessionId": message.user.sessionId,
                                           "nickname": message.user.nickname,
                                           "uuid": message.user.uuid
                                          ]
                                 ]
        socket.emit("userJoin", with: [msg])
    }
    
    func newUserConnect(handler: @escaping (_ message: Message) -> Void){
        socket.on("newUser"){ (data, ack) in
            
            let msg = data[0] as! [String: Any]
            let usr = msg["user"] as! [String: Any]
            let user = User(sessionId: usr["sessionId"] as! String, uuid: usr["uuid"] as! String, nickname: usr["nickname"] as! String)
            let message = Message(type: 3, user: user, message: "none", timestamp: msg["timestamp"] as! String)
            handler(message)
            
        }
    }
    
    func userleftDisconnect(message: Message) {
    let msg: [String: Any] = [
                                  "type": 4,
                                  "timestamp": "none",
                                  "user": ["sessionId": message.user.sessionId,
                                           "nickname": message.user.nickname,
                                           "uuid": message.user.uuid
                                          ]
                                 ]
        socket.emit("userLeft", with: [msg])
    }
    
    func userLeftMessage(handler: @escaping (_ message: Message) -> Void){
        socket.on("leftUser"){ (data, ack) in
            
            let msg = data[0] as! [String: Any]
            let usr = msg["user"] as! [String: Any]
            let user = User(sessionId: usr["sessionId"] as! String, uuid: usr["uuid"] as! String, nickname: usr["nickname"] as! String)
            let message = Message(type: 4, user: user, message: "none", timestamp: msg["timestamp"] as! String)
            handler(message)
            
        }
    }
    

    
    func handleNewMessage(handler: @escaping (_ message: Message) -> Void) {
        socket.on("newMessage") { (data, ack) in
            let msg = data[0] as! [String: Any]
            let usr = msg["user"] as! [String: Any]
            let user = User(sessionId: usr["sessionId"] as! String, uuid: usr["uuid"] as! String, nickname: usr["nickname"] as! String)
            let message = Message(type: 2, user: user, message: msg["message"] as! String, timestamp: msg["timestamp"] as! String)
            handler(message)
        }
    }
    
    

    
    func sendMessage(message: Message) {
        let msg: [String: Any] = [
                                  "type": 2,
                                  "timestamp": message.timestamp,
                                  "message": message.message,
                                  "user": ["sessionId": message.user.sessionId,
                                           "nickname": message.user.nickname,
                                           "uuid": message.user.uuid
                                          ]
                                 ]
        socket.emit("sendMessage", with: [msg])
    }
    
    
    
    
    
    // socket connection completion
    
    func didConnect(handler: @escaping () -> Void){
        socket.on(clientEvent: .connect) { (data, ack) in
            print("App Chat: socket connected")
            handler()
        }
    }
    
    func didDisConnect(handler: @escaping () -> Void){

        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("App Chat: Disconnect")
            handler()
        }
    }
    /*
    socket.on(clientEvent: .error) { (data, ack) in
        if let errorStr: String = (data[0] as? String) {
            if errorStr.hasPrefix("ERR_SOCKETIO_INVALID_SESSION") {
                self.manager.disconnect()
                print("App Chat: error \(errorStr)")
            }
        }
    }*/
    func didReConnect(handler: @escaping () -> Void){
        socket.on(clientEvent: .reconnect) { (data, ack) in
            print("App Chat: reconnect")
            handler()
        }
    }
    
    func didReconnectAttemp(handler: @escaping () -> Void){
        socket.on(clientEvent: .reconnectAttempt) { (data, ack) in
            print("App Chat: reconnectAttempt")
            handler()
        }
    }
    
}
