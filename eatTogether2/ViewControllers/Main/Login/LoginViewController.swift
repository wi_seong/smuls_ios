//
//  LoginViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class LoginViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //realm swift
    let realm = try! Realm()
    
    // Variables
    
    
    //Outlets
    //email
    @IBOutlet weak var emailText: UITextField!
    //password
    @IBOutlet weak var passText: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    
    override func loadView() {
        super.loadView()
        
        setupLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 스와이프로 뒤로가기 허가
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self

        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailText.endEditing(true)
        passText.endEditing(true)
    }
    
    @IBAction func nextPassword(_ sender: Any) {
        
        emailText.resignFirstResponder()
        passText.becomeFirstResponder()
    }

    @IBAction func GoClicked(_ sender: Any) {
        
        passText.endEditing(true)
        loginAction(){
            // 다음화면 넘어가기
            self.goToTab()
        }
    }
    
    
    
    // MARK: - Buttons Action --------------------//
    
    @IBAction func loginButtonAction(_ sender: Any) {
        
        //로그인 버튼 눌렀을 때 실행하는 함수
        loginAction(){
            // 다음 화면 넘어가기
            self.goToTab()
        }
        
    }
    

    
    
    
    // ------------------------------------------ //
    
    // MARK: - Functions
    
    private func setupLabels(){
        //네비게이션 바 보이기
        navigationController?.isNavigationBarHidden = false
        
        //로그인 버튼 셋팅
        loginButton.layer.cornerRadius = 5.0
        loginButton.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        loginButton.tintColor = UIColor.black
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.black.cgColor
        
        emailText.layer.borderWidth = 1
        emailText.layer.borderColor = UIColor.gray.cgColor
        emailText.layer.cornerRadius = 5
        
        passText.layer.borderWidth = 1
        passText.layer.borderColor = UIColor.gray.cgColor
        passText.layer.cornerRadius = 5
        /*
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "question"), style: .done, target: self, action: #selector(rightNavTapped))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.lightGray
        */
        
        
        
        
    }
    

    /*
    @objc func rightNavTapped(sender: UIBarButtonItem){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "가입 안내"
        vc.thisUrl = "https://smuls.com/join.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }*/
    

    
    func loginSuccess(_ email: String, _ univ: String){
        
        // 로그인 성공하게 되면, 대학정보를 DB에서 불러오기
        let body: Parameters = [
            "univ": univ,
        ]

        let url = dbInfo.secure_url + "api/query/myUniv"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            
            switch result{
            case 201:
                //로그인 성공했을 때
                
                let campus_count = json["campus_count"].intValue
                let univ_list_kr = json["univ_list_kr"].stringValue
                let campus_type = json["campus_type"].array // myUniv api에서는 campus_type, login에서는 campus_list
                
                
                // 대학교 정보 저장
                // realm에 저장하기 (localDB에 저장)
                try! self.realm.write{
                    self.realm.create(UnivDTO.self, value: [
                        "univ_list_en" : univ,
                        "univ_list_kr" : univ_list_kr,
                        "campus_count" : campus_count
                
                    ])
                }
                
                //캠퍼스 정보 저장(여러개일 경우 for문을 통해 여러개 저장)
                for idx in 0..<campus_type!.count{
                    let num = campus_type![idx]["num"].intValue
                    let name = campus_type![idx]["name"].stringValue
                    
                    //print("\(num) \(name)")
                    // realm에 저장하기 (localDB에 저장)
                    try! self.realm.write{
                        self.realm.create(campusDTO.self, value: [
                            "num" : num,
                            "name" : name
                    
                        ])
                    }
                }
                
                
                // 마지막으로 다음화면으로 넘어가기
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "loginToTab", sender: nil)
                }
                break
                
            case 202:
                self.OKDialog("회원님의 대학교 정보를 불러오지 못했습니다.")
                break
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
        }
        
        

        
    }
    
    func goToTab(){
        // 마지막으로 다음화면으로 넘어가기
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "loginToTab", sender: nil)
        }
    }
    
    func loginAction(handler: @escaping () -> Void){
        
            let email = emailText.text!
            let pass = passText.text!
            
            
            
            if email.isEmpty || pass.isEmpty{
                
                OKDialog("빈 칸을 채워주세요")
        
                return
            }
            
            if !isValidEmail(email){
                //email 형식 실패 안내
                OKDialog("캠퍼스 메일을 입력해주세요!")

                return
                
            }
            if !isValidPass(pass){
                //passworkd 형식 안내
                
                OKDialog("8~15자리 비밀번호를 입력해주세요.")
                passText.text = nil
               
                return
                
            }

            
            
            
            //let concurrentDispatch = DispatchQueue(label: "registerUser", qos: .background, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil)
            
            /********************** 서버에 전송하기 ************************/
            
            //멀티 스레드로 효율적으로 수행
            //concurrentDispatch.async {
                //print(SelfUser.device_id)
                //print(SelfUser.push_token)
        //print("디바이스아이디: \(UIDevice.current.identifierForVendor!.uuidString)")
        
        sessionInfo.device_id = UIDevice.current.identifierForVendor!.uuidString
        
        print("디바이스 아이디: \(sessionInfo.device_id)")
        
                let body: Parameters = [
                    "email": email,
                    "pwd": pass,
                    "device_id": sessionInfo.device_id,
                    "device_os": "iOS",
                    "push_token": sessionInfo.pushToken
                ]

                let url = dbInfo.secure_url + "api/auth/login"
                Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
                    
                    let result = json["result"].intValue
                    //print(json)
                    switch result{
                    case 201:
                        //로그인 성공했을 때
                        let user = SelfUser()
                        
                        user._id = json["_id"].stringValue
                        user.uuid = json["uuid"].stringValue
                        user.nick_name = json["nick_name"].stringValue
                        user.email = email
                        let univ = json["univ"].stringValue
                        user.univ = univ
                        user.campus_type = json["campus_type"].intValue
                        user.gender = json["gender"].stringValue
                        user.name = json["name"].stringValue
                        user.introduce = json["introduce"].stringValue
                        user.manner_score = json["manner_score"].doubleValue
                        user.introduce = json["introduce"].stringValue
                        user.is_blocked = json["is_blocked"].boolValue
                        user.coin = json["coin"].intValue
                        user.push_token = sessionInfo.pushToken
                    
                        user.device_id = UIDevice.current.identifierForVendor!.uuidString
                        
                        // realm에 저장하기 (localDB에 저장)
                        try! self.realm.write{
                            self.realm.add(user)
                        }
                        
                        
                        // <- 대학 정보 로컬데이터베이스에 저장하기 ->
                        let campus_count = json["campus_count"].intValue
                        let univ_list_kr = json["univ_list_kr"].stringValue
                        let campus_list = json["campus_list"].array // myUniv api에서는 campus_type, login에서는 campus_list
                        
                        
                        // 대학교 정보 저장
                        // realm에 저장하기 (localDB에 저장)
                        try! self.realm.write{
                            self.realm.create(UnivDTO.self, value: [
                                "univ_list_en" : univ,
                                "univ_list_kr" : univ_list_kr,
                                "campus_count" : campus_count
                        
                            ])
                        }
                        
                        //캠퍼스 정보 저장(여러개일 경우 for문을 통해 여러개 저장)
                        for idx in 0..<campus_list!.count{
                            let num = campus_list![idx]["num"].intValue
                            let name = campus_list![idx]["name"].stringValue
                            
                            //print("\(num) \(name)")
                            // realm에 저장하기 (localDB에 저장)
                            try! self.realm.write{
                                self.realm.create(campusDTO.self, value: [
                                    "num" : num,
                                    "name" : name
                            
                                ])
                            }
                        }
                        
                        // 다음화면 넘어가기
                        handler()
                        
                        
                        //다음 화면으로 넘어가기 2021년 2월 12일 심사테스트 이후 변경
                        //self.loginSuccess(email, univ)
                    
                        break
                        
                    case 202:
                        self.OKDialog("계정이 없거나 비밀번호가 일치하지 않습니다!")
                        break
                    case 203:
                        self.OKDialog("이메일 인증을 완료해주세요:)\n스팸메일함에 수신될 수 있습니다.")
                        break
                    case 204:
                        self.OKDialog("회원탈퇴하신 기록이 있습니다\n계정을 활성화하시겠습니까?")
                        break
                    case 205:
                        self.OKDialog("다른 기기에 접속 시\n 기기변경을 신청해주세요!")
                        break
                    case 206:
                        self.OKDialog("관리자에 의해 계정이 정지되었습니다.\n플러스친구 스물스에 문의 바랍니다.")
                        break
                        
                    default:
                        self.OKDialog("알 수 없는 에러")
                        break
                    }
                }
                /********************** ********** ************************/
                
            //}
        
        
    }
    
    
    
    
    func isValidEmail(_ email: String) -> Bool{
        
        //이메일 가능한지 확인하기
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func isValidPass(_ pass: String) -> Bool{
        let passRegEx = "^[a-zA-Z0-9#?!@$%^&*-]{8,15}$"
        let passTest = NSPredicate(format: "SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: pass)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //button action
    
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
