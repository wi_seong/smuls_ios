//
//  FindPassWordController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/29.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import Alamofire

class FindPassWordController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    
    
    @IBOutlet weak var FindButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        debugLabels()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailText.endEditing(true)
    }
    
    
    // MARK - ACTIONS
    @IBAction func FindAction(_ sender: Any) {
        FindAction()
    }
    @IBAction func GoClicked(_ sender: Any) {
        emailText.endEditing(true)
        FindAction()
    }
    
    func debugLabels(){
        
        FindButton.layer.cornerRadius = 5.0
        FindButton.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        FindButton.tintColor = UIColor.black
        FindButton.layer.borderColor = UIColor.black.cgColor
        FindButton.layer.borderWidth = 1
        
        emailText.layer.borderWidth = 1
        emailText.layer.borderColor = UIColor.gray.cgColor
        emailText.layer.cornerRadius = 5
        
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func FindAction(){
        
        let email = emailText.text!
        
        if email.isEmpty{
            OKDialog("빈칸을 채워주세요")
            return
        }
        
        let body: Parameters = [
            "email": email
        ]

        let url = dbInfo.secure_url + "pwf"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            
            switch result{
            case 101:
                self.OKDialog("비밀번호 찾기\n인증메일이 전송되었습니다!\n스팸메일함에 수신될 수 있습니다.")
                self.emailText.text = nil
                self.FindButton.isEnabled = false
                break
            case 201:
                self.OKDialog("10분 후에 다시 이용할 수 있습니다")
                self.emailText.text = nil
                self.FindButton.isEnabled = false
                break
            case 401:
                self.OKDialog("이메일이 불일치합니다")
                self.emailText.text = nil
                
                break
            default:
                self.OKDialog("알 수 없는 에러")
                self.emailText.text = nil
                self.FindButton.isEnabled = false
                break
            }
        }
        
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
