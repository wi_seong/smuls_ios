//
//  ExplainViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ExplainViewController: UIViewController, UIScrollViewDelegate {
    
    
    //Lables
    
    //로그인 버튼
    @IBOutlet weak var LoginButton: UIButton!
    //회원가입 버튼
    @IBOutlet weak var RegisterButton: UIButton!
    //페이지 컨트롤
    //@IBOutlet weak var pageControl: UIPageControl!
    //스크롤뷰 아울렛
    //@IBOutlet weak var ScrollView: UIScrollView!
    /*
    //images
    var images = ["main_asset_indicator_full.png", "main_asset_plaintext_1.png"]
    var frame = CGRect(x: 0, y:0, width: 0, height: 0)
    
    //constant
    let numOfTouches = 2
    
    //variables
    private var shadowImageView: UIImageView?
    
    */
    
    private var player: AVPlayer?
    
    
    override func loadView() {
        super.loadView()
        
        setupLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //네비게이션 바 감추기
        //navigationController?.isNavigationBarHidden = true
        /*
        if shadowImageView == nil {
            shadowImageView = findShadowImage(under: navigationController!.navigationBar)
        }
        shadowImageView?.isHidden = true
        */
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //shadowImageView?.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // view 셋업
        //setupView()

        // Do any additional setup after loading the view.
    }
    
    func setupView(){

        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "smuls_main", ofType: "mov")!)
        player = AVPlayer(url: path)
        
        let newLayer = AVPlayerLayer(player: player)
        newLayer.frame = self.view.frame
        self.view.layer.addSublayer(newLayer)
        newLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        player!.play()
        
        player!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        /*
        NotificationCenter.default.addObserver(self, selector: #selector(videoDidPlayToEnd(_:)), name: Notification.Name(rawValue: "AVPlayerItemToEndTimeNotification"), object: player!.currentItem)
        */
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { [weak self] _ in
            self?.player?.seek(to: CMTime.zero)
            self?.player?.play()
  
        }
        
        view.addSubview(LoginButton)
        view.addSubview(RegisterButton)
    }
    /*
    @objc func videoDidPlayToEnd(_ notification: Notification){
        //let player: AVPlayerItem = notification.object as! AVPlayerItem
        player!.seek(to: CMTime.zero)
        player!.play()
    }*/

    
    
    
    
    // MARK: - 레이아웃 셋팅
    
    func setupLabels(){
        
        //네비게이션 바 감추기
        //navigationController?.isNavigationBarHidden = true
        
        //로그인 버튼 셋팅
        LoginButton.layer.cornerRadius = 5.0
        LoginButton.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        LoginButton.layer.borderWidth = 1
        LoginButton.layer.borderColor = UIColor.black.cgColor
        LoginButton.tintColor = UIColor.black
        
        //회원가입 버튼 셋팅
        RegisterButton.layer.cornerRadius = 5.0
        RegisterButton.backgroundColor = UIColor.white
        RegisterButton.tintColor = UIColor.black
        RegisterButton.layer.borderColor = UIColor.black.cgColor
        RegisterButton.layer.borderWidth = 1
        
        
        //Page Controls 셋팅
        /*
        pageControl.numberOfPages = images.count
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = UIColor.yellow
        
        
        for index in 0..<images.count{
            frame.origin.x = ScrollView.frame.size.width * CGFloat(index)
            frame.size = ScrollView.frame.size
            
            let imgView = UIImageView(frame: frame)
            imgView.image = UIImage(named: images[index])
            self.ScrollView.addSubview(imgView)
        }
        
        ScrollView.contentSize = CGSize(width: (ScrollView.frame.size.width * CGFloat(images.count)), height: ScrollView.frame.size.height)
        
        ScrollView.delegate = self
        
        */

        
    }
    /*
    private func findShadowImage(under view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1 {
            return (view as! UIImageView)
        }
        
        for subview in view.subviews {
            if let imageView = findShadowImage(under: subview) {
                return imageView
            }
        }
        return nil
    }
    
    */
    // MARK: - Button Actions ------------------ //
/*
    @IBAction func lookAroundAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: true)
            //self.dismissViewControllerAnimated(false, completion: {})
            self.performSegue(withIdentifier: "tabSegue", sender: nil)
            
            
        }
    }
    */
    
    // ----------------------------------------- //
    
    
    /*
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        
        pageControl.currentPage = Int(pageNumber)
    }
    
*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
