//
//  SplashViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import FirebaseFirestore
import RealmSwift

class SplashViewController: UIViewController {
    
    let appVersion: String = "ios_101"
    
    @IBOutlet weak var splashImg: UIImageView!
    
    override func loadView() {
        super.loadView()
        setSplashImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
        
        
        
        // 상용화 했을 때 주석 풀기!
        
        versionCheck()
        

        
        
    }
    // status bar 없애기
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    private func setSplashImage(){
        
        
        let num = Int.random(in: 1...3)
        let str = "splash" + String(num)
        
        splashImg.image = UIImage(named: str)
    }
    
    func initialization(){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        //sessionInfo.device_id = UIDevice.current.identifierForVendor!.uuidString
        
        if selfuser.count != 0 {
            //다음 화면으로 넘어가기
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)){
                //self.performSegue(withIdentifier: "TempSegue", sender: nil)
                
                print("이메일은 \(selfuser[0].email)")
                // 로그인 정보가 있으면 tab있는 화면으로 넘어가기
                self.performSegue(withIdentifier: "MainSegue", sender: nil)
                
                
            }
            
        }
        else{
            //설명해주는 화면으로 넘어가기
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)){
                //self.performSegue(withIdentifier: "TempSegue", sender: nil)
                self.performSegue(withIdentifier: "ExplainSegue", sender: nil)
                
                
            }
            
        }


        
        
    }
    
    
    func versionCheck(){
        
        
        
        let docRef = Firestore.firestore().collection("versions").document("\(appVersion)")
        
        docRef.getDocument{ (snapshot, err) in
            if let data = snapshot?.data() {
                let status = Int((data["status"] as? Int)!)
                let exp = data["explain"] as? String
                
                
                if status != 1{ // 1이 아닐땐, 서버에서 문제가 있거나 점검중 혹은 지원하지 않는 버전
                    let alert = UIAlertController(title: "", message: exp, preferredStyle: .alert)
                    let success = UIAlertAction(title: "확인", style: .default, handler: {
                        _ in
                        
                        exit(0) // 종료시켜주기
                    })
                    alert.addAction(success)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
                else{
                    // 초기화
                    self.initialization()
                    
                }
                
                
                

                
                
            }
            else{
                self.Toast(message: "앱 버전 정보를 가져오지 못했습니다.\n")
            }
            
        }
        
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
