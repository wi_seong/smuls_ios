//
//  MainWebViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/29.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import WebKit

class MainWebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var thisUrl: String?
    var thisTitle: String?

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        initialize()
        // Do any additional setup after loading the view.
    }
    
    private func initialize(){
        
        view.addSubview(webView)
        
        let url = URL(string: thisUrl!)!
        let request = URLRequest(url: url)
        
        webView.load(request)
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.title = thisTitle!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
