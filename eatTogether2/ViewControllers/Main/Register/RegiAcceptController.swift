//
//  RegiAcceptController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/29.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class RegiAcceptController: UIViewController {

    
    @IBOutlet weak var AcceptLabel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        debugLabels();
    }
    
    // MARK: - Functions
    
    private func debugLabels(){
        AcceptLabel.layer.cornerRadius = 5.0
        AcceptLabel.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        AcceptLabel.tintColor = UIColor.black
        AcceptLabel.layer.borderWidth = 1
        AcceptLabel.layer.borderColor = UIColor.black.cgColor
        
        self.title = "회원가입"
    }
    
    // MARK: - ACTIONS
    
    
    @IBAction func AcceptAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func termsAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "이용약관"
        vc.thisUrl = "https://smuls.com/TermsOfService.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func personalAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "개인정보 처리방침"
        vc.thisUrl = "https://smuls.com/PrivacyPolicy.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
