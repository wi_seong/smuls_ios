//
//  RegisterViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import DLRadioButton

class RegisterViewController: UIViewController, UIGestureRecognizerDelegate {

    // Variables
    private var shadowImageView: UIImageView?
    
    //Outlets
    @IBOutlet weak var emailText: UITextField!
    //pass
    @IBOutlet weak var PassText: UITextField!
    //confirm pass
    @IBOutlet weak var confirmPassText: UITextField!
    //register button
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var doubleCheckText: UITextField!
    
    @IBOutlet weak var doubleCheckLabel: UIButton!
    
    @IBOutlet weak var termsLabels: DLRadioButton!
    
    @IBOutlet weak var personalLabel: DLRadioButton!
    
    
    var isCheckedNickName = false
    var nickName = ""
    
    override func loadView() {
        super.loadView()
        
        setupLabels()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 스와이프로 뒤로가기 허가
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        // 알파벳키보드로 변경
        PassText.keyboardType = UIKeyboardType.alphabet
        confirmPassText.keyboardType = UIKeyboardType.alphabet
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
 
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailText.endEditing(true)
        PassText.endEditing(true)
        confirmPassText.endEditing(true)
        doubleCheckText.endEditing(true)
    }
    
    @IBAction func nextPassword(_ sender: Any) {
        
        emailText.resignFirstResponder()
        PassText.becomeFirstResponder()
    }
    @IBAction func continuePass(_ sender: Any) {
        
        PassText.resignFirstResponder()
        confirmPassText.becomeFirstResponder()
    }


    @IBAction func GoClicked(_ sender: Any) {
        
        confirmPassText.endEditing(true)
        registerAction()
    }
    
    @IBAction func termsContent(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "이용약관"
        vc.thisUrl = "https://smuls.com/TermsOfService.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func personalContent(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "개인정보 처리방침"
        vc.thisUrl = "https://smuls.com/PrivacyPolicy.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    
    // MARK: - Buttons Action --------------------//
    
    @IBAction func registerButtonAction(_ sender: Any) {
        registerAction()
    }
    
    @IBAction func doubleCheckAction(_ sender: Any) {
        doubleCheckAction()
    }
    
    
    // MARK: - Functions
    
    
    private func setupLabels(){
        
        //네비게이션 바 보이기
        navigationController?.isNavigationBarHidden = false
        
        //회원가입 버튼 셋팅
        registerButton.layer.cornerRadius = 5.0
        registerButton.backgroundColor = UIColor(red: 233/255, green: 247/255, blue: 227/255, alpha: 1)
        registerButton.tintColor = UIColor.black
        registerButton.layer.borderColor = UIColor.black.cgColor
        registerButton.layer.borderWidth = 1
        
        emailText.layer.borderWidth = 1
        emailText.layer.borderColor = UIColor.gray.cgColor
        emailText.layer.cornerRadius = 5
        
        PassText.layer.borderWidth = 1
        PassText.layer.borderColor = UIColor.gray.cgColor
        PassText.layer.cornerRadius = 5
        
        confirmPassText.layer.borderWidth = 1
        confirmPassText.layer.borderColor = UIColor.gray.cgColor
        confirmPassText.layer.cornerRadius = 5
        
        doubleCheckText.layer.borderWidth = 1
        doubleCheckText.layer.borderColor = UIColor.gray.cgColor
        doubleCheckText.layer.cornerRadius = 5
        
        doubleCheckLabel.layer.borderWidth = 1
        doubleCheckLabel.layer.borderColor = UIColor.gray.cgColor
        doubleCheckLabel.layer.cornerRadius = 5
        
        // 체크 해제 가능하도록
        termsLabels.isMultipleSelectionEnabled = true
        personalLabel.isMultipleSelectionEnabled = true
        /*
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "question"), style: .done, target: self, action: #selector(rightNavTapped))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.lightGray
        */
    }
    /*
    @objc func rightNavTapped(sender: UIBarButtonItem){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MainWebViewController") as! MainWebViewController
        vc.thisTitle = "가입 안내"
        vc.thisUrl = "https://smuls.com/join.html"
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }*/

    

    func registerSuccess(_ email: String){
        
        
        emailText.text = nil
        PassText.text = nil
        confirmPassText.text = nil
        doubleCheckText.text = nil
        registerButton.isEnabled = false
        termsLabels.isSelected = false
        personalLabel.isSelected = false
        
        OKDialog("회원가입 완료!\n인증메일이 전송되었습니다!\n스팸메일함에 수신될 수 있습니다.")
        
        
        
        
        
    }
    
    func registerAction(){
        
        if !(personalLabel.isSelected && termsLabels.isSelected){
            OKDialog("필수 항목 동의 후 가입 가능합니다")
            return
        }
        
        let email = emailText.text!
        let pass = PassText.text!
        let confirm = confirmPassText.text!
        
        if !isCheckedNickName{
            OKDialog("닉네임 중복확인을 해주세요")
    
            return
        }
        
        if email.isEmpty || pass.isEmpty || confirm.isEmpty{
            
            OKDialog("빈 칸을 채워주세요")
    
            return
        }
        
        if !isValidEmail(email){
            //email 형식 실패 안내
            OKDialog("캠퍼스 메일을 입력해주세요!")

            return
            
        }
        if !isValidPass(pass){
            //passworkd 형식 안내
            
            OKDialog("8~15자리 비밀번호를 입력해주세요.")
            PassText.text = nil
            confirmPassText.text = nil
            
            return
            
        }

        if pass != confirm{
            
            OKDialog("비밀번호가 일치하지 않습니다.")
            PassText.text = nil
            confirmPassText.text = nil
            
            return
        }
        
        
        
        //let concurrentDispatch = DispatchQueue(label: "registerUser", qos: .background, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil)
        
        /********************** 서버에 전송하기 ************************/
        
        //멀티 스레드로 효율적으로 수행
        //concurrentDispatch.async {
            //print(SelfUser.device_id)
            //print(SelfUser.push_token)
        
        sessionInfo.device_id = UIDevice.current.identifierForVendor!.uuidString
        
        //print("디바이스아이디: \(sessionInfo.device_id)")
            
            let body: Parameters = [
                "email": email,
                "pwd": pass,
                "device_id": sessionInfo.device_id,
                "device_os": "iOS",
                "push_token": sessionInfo.pushToken,
                "nick_name": nickName,
                "ad_receive_consent": "true"
            ]

            let url = dbInfo.secure_url + "api/auth/register"
            Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
                
                let result = json["result"].intValue
                
                switch result{
                case 101:
                    self.registerSuccess(email)
                    break
                case 102:
                    self.OKDialog("아직 지원하지 않는 캠퍼스입니다.\n지원하는 캠퍼스를 참고해주세요 :)")
                    break
                case 103:
                    self.OKDialog("이미 가입되어 있습니다")
                    break
                default:
                    self.OKDialog("알 수 없는 에러")
                    break
                }
            }
            /********************** ********** ************************/
            
        //}
        
    }
    

    func isValidEmail(_ email: String) -> Bool{
        
        //이메일 가능한지 확인하기
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func isValidPass(_ pass: String) -> Bool{
        let passRegEx = "^[a-zA-Z0-9#?!@$%^&*-]{8,15}$"
        let passTest = NSPredicate(format: "SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: pass)
    }
    
    
    func doubleCheckAction(){
        let doubleCheck = doubleCheckText.text!
       
        
        
        if doubleCheck.isEmpty{
            
            OKDialog("빈 칸을 채워주세요")
    
            return
        }
        

    
        
        //print("디바이스아이디: \(sessionInfo.device_id)")
            
            let body: Parameters = [
                "nick_name": doubleCheck
            ]

            let url = dbInfo.secure_url + "api/auth/nickNameCheck"
            Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
                
                let result = json["result"].intValue
                
                switch result{
                case 101:
                    self.OKDialog("사용가능합니다")
                    self.isCheckedNickName = true
                    self.nickName = doubleCheck
                    break
                case 102:
                    self.OKDialog("닉네임이 이미 존재합니다")
                    self.isCheckedNickName = false
                    break
                case 103:
                    self.OKDialog("띄어쓰기를 제외한\n10자 이내의 한글/영어를 사용해주세요")
                default:
                    self.OKDialog("알 수 없는 에러")
                    break
                }
            }
            /********************** ********** ************************/
            
        //}
        
    }
    
    // ------------------------------------------ //
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Button Actions
    
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
