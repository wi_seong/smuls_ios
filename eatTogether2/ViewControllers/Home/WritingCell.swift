//
//  writingCell.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/08/08.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class WritingCell:UICollectionViewCell{

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var explanation: UILabel!
    
    @IBOutlet weak var information1: UILabel!
    
    @IBOutlet weak var information2: UILabel!
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var background: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.thumbnail.layer.cornerRadius = 5
        
    }
    
}
