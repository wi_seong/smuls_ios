//
//  roomInfo.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/25.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import SwiftyJSON
struct RoomInfo
{
    
    var isPassed = false
    var room_id = ""
    var img: UIImage?
    var room_name = ""
    var activity_explain = ""
    var appointed_at = ""
    var campus_type = ""
    var created_at = ""
    var current_people = ""
    var gender_fix = ""
    var how_hot = ""
    var leader:JSON
    var max_people = ""
    var members:JSON
    var reported_count = ""
    var updated_at = ""
    var location = ""
    
    init(_ data: JSON)
    {
        
        self.room_id = data["room_id"].stringValue
        
        
        let t1 = data["img"].stringValue
        
        let url = URL(string: t1)

        if(url != nil){
            let data = try? Data(contentsOf: url!)
            if(data != nil){
                self.img = UIImage(data: data!)
            }else{
                print("image data is nil")
            }

        }
        
        
        self.room_name = data["room_name"].stringValue
        
        
        self.activity_explain = data["activity_explain"].stringValue
        
        
        let t2 = data["appointed_at"].stringValue
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: t2)!
        
        let changedDate = DateFormatter()
        
        
        changedDate.locale = Locale(identifier: "ko_KR")
        changedDate.timeZone = TimeZone(abbreviation: "KST")
    
        
        
        changedDate.dateFormat = "M.d EEEE "
        let dateOutput = changedDate.string(from: date)
        changedDate.locale = Locale(identifier: "en_US")
        changedDate.dateFormat = "h:mm a"
        let time = changedDate.string(from: date)
        let kr = dateOutput + time
        let timestamp = kr
        self.appointed_at = timestamp
        
        self.campus_type = data["campus_type"].stringValue
        
        self.created_at = data["created_at"].stringValue
        
        
        self.current_people = String(data["current_people"].intValue)
        
        
        let t3 = data["gender_fix"].stringValue
        
            if t3 == "M"{
                self.gender_fix = "Men"
            }
            else if t3 == "F"{
                self.gender_fix = "Women"
            }
            else{
                self.gender_fix = "All"
            }
            
        
        
        self.how_hot = data["how_hot"].stringValue
        
        
        self.leader = data["leader"]
 
        
        self.max_people = String(data["max_people"].intValue)
        
        
        self.members = data["members"]
        
        
        self.reported_count = data["reported_count"].stringValue
        
        self.updated_at = data["updated_at"].stringValue

        self.location = data["location"].stringValue
        
    }
    /*
    init(_ data:NSDictionary)
    {
        
        if let add = data["room_id"] as? String
        {
            self.room_id = add
        }
        
        if let add = data["img"] as? String
        {
            let url = URL(string: add)

            if(url != nil){
                let data = try? Data(contentsOf: url!)
                if(data != nil){
                    self.img = UIImage(data: data!)
                }else{
                    print("image data is nil")
                }

            }
           
        }
        
        if let add = data["room_name"] as? String
        {
            self.room_name = add
        }
        
        if let add = data["activity_explain"] as? String
        {
            self.activity_explain = add
        }
        
        if let add = data["appointed_at"] as? String
        {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            
            let date: Date = dateFormatter.date(from: add)!
            
            let changedDate = DateFormatter()
            
            
            changedDate.locale = Locale(identifier: "ko_KR")
            changedDate.timeZone = TimeZone(abbreviation: "KST")
        
            
            
            changedDate.dateFormat = "M/d(E) a h시mm분"
            let kr = changedDate.string(from: date)
            let timestamp = kr
            self.appointed_at = timestamp
            //self.appointed_at = add
        }
        
        if let add = data["campus_type"] as? String
        {
            self.campus_type = add
        }
        
        if let add = data["created_at"] as? String
        {
            self.created_at = add
        }
        
        if let add = data["current_people"] as? Int
        {
            self.current_people = String(add)
        }
        
        if let add = data["gender_fix"] as? String
        {
            if add == "M"{
                self.gender_fix = "Men"
            }
            else if add == "F"{
                self.gender_fix = "Women"
            }
            else{
                self.gender_fix = "All"
            }
            
        }
        
        if let add = data["how_hot"] as? String
        {
            self.how_hot = add
        }
        
        if let add = data["leader"] as? NSDictionary
        {
            self.leader = add
        }
        
        if let add = data["max_people"] as? Int
        {
            self.max_people = String(add)
        }
        
        if let add = data["members"] as? Array<Any>
        {
            
            self.members = add
            print("멤버데이터: \(self.members)")
        }
        
        if let add = data["reported_count"] as? String
        {
            self.reported_count = add
        }
        
        if let add = data["updated_at"] as? String
        {
            self.updated_at = add
        }
        if let add = data["location"] as? String{
            self.location = add
        }
        
    }*/

}
