//
//  HomeViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/26.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
import DateTimePicker
import AYPopupPickerView
import Alamofire

class HomeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = CGPoint(x: view.frame.size.width  / 2,
                                           y: view.frame.size.height / 2 - 50)
        activityIndicator.hidesWhenStopped = false
        activityIndicator.style = .large
    
        return activityIndicator
        
    }()
    
    @IBOutlet var FirstParentView: UIView!

    @IBOutlet weak var tableView: UITableView!
    //Filter
    @IBOutlet weak var filterParentLayout: UIView!
    
    @IBOutlet weak var filterLayout: UIView!
        
    @IBOutlet weak var category_eat_layout: UIButton!
    
    @IBOutlet weak var category_learn_layout: UIButton!
    
    @IBOutlet weak var category_play_layout: UIButton!
    
    @IBOutlet weak var category_etc_layout: UIButton!
    
    @IBOutlet weak var date_button_layout: UIButton!
    
    @IBOutlet weak var filterSearchLayout: UIButton!
    
    var nullMessage: UILabel?
    var RoomList = [RoomInfo]()
    
    var cat0 = false
    var cat1 = false
    var cat2 = false
    var cat3 = false
    
    var appointed_at:String = ""
    //Filter - category
    
    var CATEGORY = Array<Int>() // [ 순서대로 0, 1, 2, 3...] 배열로  넣기
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 스와이프로 뒤로가기 허가
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
        //delegates
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        debugLabelsAndLayout()
        
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.hidesWhenStopped = true
        
        self.activityIndicator.startAnimating()
        //dateView restrict past
        //meetDateLayout.minimumDate = Date()
    
        
        //show tabbar
        //self.tabBarController?.tabBar.isHidden = false
        
        // 새로고침 추가
        initRefresh()
        
        // 동시 큐
        concurrentWorkItems()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // 새로운 뉴스가 있었다면 새로고침 해주기
        investigateNews()
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        
        filterParentLayout.isHidden = true
        
        //show tabbar
        self.tabBarController?.tabBar.isHidden = false

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    private func investigateNews(){
        if sessionInfo.isRequiredToRefreshHome{
            self.activityIndicator.startAnimating()
            getRoomInitially {
                self.activityIndicator.stopAnimating()
                sessionInfo.isRequiredToRefreshHome = false
            }
        }
    }
    
    private func concurrentWorkItems(){
        let ConcurrentQueue = DispatchQueue(label: "comparePushToken", qos: .background, attributes: [.concurrent], autoreleaseFrequency: .workItem, target: nil)
        
        ConcurrentQueue.async {
            // 푸쉬토큰 바껴있다면 업데이트!
            self.comparePushToken()
        }
    }
    
    // 알림 거부했다가 다시 허용했다면 새로운 푸쉬 토큰 업로드 해주기
    private func comparePushToken(){
        
        
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        if sessionInfo.pushToken != "" && selfuser[0].push_token != sessionInfo.pushToken{ // token값이 nil이 아니고, db에 사용자 정보가 있고, 사용자 정보의 token과, 할당받은pushToken이 다를 때

            //새로운 PushToken업로드 해주기
            
            let uuid = selfuser[0].uuid
            let email = selfuser[0].email
            
            
            let body: Parameters = [
                "uuid": uuid,
                "pushToken": sessionInfo.pushToken,
                "os": "iOS"
            ]

            let url = dbInfo.secure_url + "api/query/changePushToken"
            Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
                
                let result = json["result"].intValue
                
                switch result{
                case 101:
                    // 푸시 변경 정상 처리
                    // realm db에도 변경
                    try! realm.write {
                        realm.create(SelfUser.self, value: ["email": email, "push_token" : sessionInfo.pushToken], update: .modified)
                   
                    }
                    
                    break
                case 111:
                    self.OKDialog("에러 발생 103\n지속적으로 발생하면 문의해주세요 :)")
                    break
                default:
                    self.OKDialog("알 수 없는 에러")
                    break
                }
            }

        }
        
    }
    
    func clearBackground(){
        
        category_eat_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        category_learn_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        category_play_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        category_etc_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        
    }

    
    func filtering(){
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let url = dbInfo.service_url + "api/roomFilter"
        
        let body: Parameters = ["univ": selfuser[0].univ,
                    "type": String(selfuser[0].campus_type),
                    "date": appointed_at,
                    "category": CATEGORY
        ] 
            
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in
            
            self.activityIndicator.stopAnimating()
            
            let array = json.array!
            
            self.RoomList.removeAll()
            

            self.nullMessage!.isHidden = true
            
            for i in 0 ..< array.count
            {
                
                //let data = self.convertToDictionary(text: array[i].rawString()!)! as NSDictionary
                self.RoomList.append(RoomInfo(array[i]))
                
            }
            self.RoomList.reverse()
            
            self.filterParentLayout.isHidden = true
            //hide border
            //self.tabBarController!.tabBar.layer.borderWidth = 0.50
            self.tabBarController!.tabBar.layer.borderColor = UIColor.clear.cgColor
            self.tabBarController?.tabBar.clipsToBounds = true
            
            //hide tabbar
            self.tabBarController?.tabBar.isHidden = false
            
            self.tableView.reloadData()
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
            
            if array.isEmpty{
                self.whenDataIsNull()

                return
            }
        }
    }

    @objc func filterClicked(){
        
        if(filterParentLayout.isHidden){
            filterParentLayout.isHidden = false
            //hide border
            //self.tabBarController!.tabBar.layer.borderWidth = 0.50
            self.tabBarController!.tabBar.layer.borderColor = UIColor.clear.cgColor
            self.tabBarController?.tabBar.clipsToBounds = true
            
            //hide tabbar
            self.tabBarController?.tabBar.isHidden = true
            
            
            // 약속시간 초기화
            self.appointed_at = ""
            date_button_layout.setTitle("날짜", for: .normal)
        }
        
    }
 

    
    
    private func initRefresh(){
        let refresh = UIRefreshControl()
        
        refresh.addTarget(self, action: #selector(updateUI(refresh:)), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "불러오는 중..")
        
        if #available(iOS 10.0, *){
            tableView.refreshControl = refresh
        }else{
            tableView.addSubview(refresh)
        }
    }
    @objc func updateUI(refresh: UIRefreshControl){
        //tableView.beginUpdates()
        getRoomInitially(){
            
            self.tableView.reloadData()
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
           // self.tableView.endUpdates()
            refresh.endRefreshing()
            
        }
        
        
    }
    
    // Functions
    
    private func debugLabelsAndLayout(){
        
        // add side buttons (filter , makeroom)
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "home_asset_filter"), for: .normal)
        lbutton.addTarget(self, action:#selector(filterClicked), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
        let rbutton = UIButton(type: UIButton.ButtonType.custom)
        rbutton.setImage(UIImage(named: "home_asset_makeroom"), for: .normal)
        rbutton.addTarget(self, action:#selector(makeroomClicked), for: .touchUpInside)
        rbutton.bounds = CGRect(x: 0, y: 0, width: CGFloat(25), height: CGFloat(25))
        let rbarButton = UIBarButtonItem(customView: rbutton)
        self.navigationItem.rightBarButtonItem = rbarButton
        
        //filter layout
        
        filterLayout.addRightBorderWithColor(color: UIColor.black, width: 2)
        filterLayout.addLeftBorderWithColor(color: UIColor.black, width: 2)
        
        category_eat_layout.layer.cornerRadius = 3
        category_eat_layout.layer.borderColor = UIColor.black.cgColor
        category_eat_layout.layer.borderWidth = 1
        
        category_learn_layout.layer.cornerRadius = 3
        category_learn_layout.layer.borderColor = UIColor.black.cgColor
        category_learn_layout.layer.borderWidth = 1
        
        category_play_layout.layer.cornerRadius = 3
        category_play_layout.layer.borderColor = UIColor.black.cgColor
        category_play_layout.layer.borderWidth = 1
        
        category_etc_layout.layer.cornerRadius = 3
        category_etc_layout.layer.borderColor = UIColor.black.cgColor
        category_etc_layout.layer.borderWidth = 1
        
        date_button_layout.layer.cornerRadius = 3
        date_button_layout.layer.borderColor = UIColor.black.cgColor
        date_button_layout.layer.borderWidth = 1
        
        filterSearchLayout.layer.cornerRadius = 20
        
        
        
        //tableview 밑줄 없애기
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        // 최상위 부모 view 배경색
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        
        
        
        // tab bar 밑줄 없애기
        UITabBar.appearance().layer.borderWidth = 0.0
        //UITabBar.appearance().clipsToBounds = true

        
        //학교 정보 검색 및 학교 이름을 네비게이션 타이틀에 적용
        let realm = try! Realm()
        let Univ = realm.objects(UnivDTO.self)
        
        self.navigationController?.navigationBar.topItem?.title = Univ[0].univ_list_kr
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "NotoSansCJKkr-Medium", size: 16)!]
        
        
        nullMessage = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        nullMessage!.isHidden = true
        view.addSubview(nullMessage!)
        
        //최초 방목록 가져오기
        getRoomInitially(){}
        
    }
    
    @objc func makeroomClicked(){
        
       let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
       let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddView") as! AddRoomViewController
        
        newViewController.modalPresentationStyle = .fullScreen
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    func getRoomInitially(handler: @escaping()-> Void){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let url = dbInfo.service_url + "api/room"
        
        print("캠퍼스 타입: \(String(selfuser[0].campus_type))")
        
        let body: Parameters = ["univ": selfuser[0].univ,
                    "type": String(selfuser[0].campus_type)
        ]
            
        Methods.shared.ServiceAlamoPost( url: url, body: body){ [self] (json) in
            
            self.activityIndicator.stopAnimating()
  
            self.RoomList.removeAll()
            
            let array = json.array!
            
            //print(array)
            

            self.nullMessage!.isHidden = true
            
            for i in 0 ..< array.count
            {
                
                //let data = self.convertToDictionary(text: array[i].rawString()!)! as NSDictionary
                self.RoomList.append(RoomInfo(array[i]))
                
            }
            self.RoomList.reverse() // 최신것부터 불러오기
            
            self.tableView.reloadData()
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
            
            if array.isEmpty{
                self.whenDataIsNull()
                handler()
                return
            }
            
            handler()
        }

    }
 
    private func whenDataIsNull(){
        nullMessage!.center = CGPoint(x: view.frame.size.width / 2,
                                      y: view.frame.size.height / 2 - 50)
        nullMessage!.text = "클럽을 먼저 만들어보는건 어때요?"
        nullMessage!.textColor = UIColor.lightGray
        nullMessage!.textAlignment = .center
        nullMessage!.isHidden = false
        FirstParentView.addSubview(nullMessage!)
        
    }


}

extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }

    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}

extension HomeViewController: DateTimePickerDelegate{
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        date_button_layout.setTitle(picker.selectedDateString, for: .normal)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        RoomList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: HomeRoomCell
        cell = tableView.dequeueReusableCell(withIdentifier: "roomCell") as! HomeRoomCell
    
        
        cell.introImage.image = RoomList[indexPath.row].img!
        
        cell.title.text = RoomList[indexPath.row].room_name
        cell.exp.text = RoomList[indexPath.row].activity_explain
        cell.date.text = RoomList[indexPath.row].appointed_at
        let curMemberCount = RoomList[indexPath.row].current_people
        let maxMemberCount = RoomList[indexPath.row].max_people
        let curMaxCount = curMemberCount + "/" + maxMemberCount
        cell.genderMember.text = (RoomList[indexPath.row].gender_fix + " · " + curMaxCount + "명")
  
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard (name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RoomDetailController")as! RoomDetailController
        vc.roomInfo = RoomList[indexPath.row]
        vc.members = RoomList[indexPath.row].members
        vc.leader = RoomList[indexPath.row].leader
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension HomeViewController{
    //  버튼들
    @IBAction func category_eat_clicked(_ sender: Any) {
        
        if cat0{
            CATEGORY.remove(0)
            cat0 = false
            category_eat_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        }else{
            CATEGORY.append(0)
            cat0 = true
            category_eat_layout.backgroundColor = UIColor(red: 180/255, green: 223/255, blue: 182/255, alpha: 1.0)
        }
        //clearBackground()
        
        
    }
    
    @IBAction func category_learn_clicked(_ sender: Any) {
        if cat1{
            CATEGORY.remove(1)
            cat1 = false
            category_learn_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        }else{
            CATEGORY.append(1)
            cat1 = true
            category_learn_layout.backgroundColor = UIColor(red: 180/255, green: 223/255, blue: 182/255, alpha: 1.0)
        }
        //clearBackground()
        
    }
    
    @IBAction func category_play_clicked(_ sender: Any) {
        if cat2{
            CATEGORY.remove(2)
            cat2 = false
            category_play_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        }else{
            CATEGORY.append(2)
            cat2 = true
            category_play_layout.backgroundColor = UIColor(red: 180/255, green: 223/255, blue: 182/255, alpha: 1.0)
        }
        //clearBackground()
        
    }
    
    @IBAction func category_etc_clicked(_ sender: Any) {
        if cat3{
            CATEGORY.remove(3)
            cat3 = false
            category_etc_layout.backgroundColor = UIColor(red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        }else{
            CATEGORY.append(3)
            cat3 = true
            category_etc_layout.backgroundColor = UIColor(red: 180/255, green: 223/255, blue: 182/255, alpha: 1.0)
        }
        //clearBackground()
        
    }
    
    @IBAction func filterSearchClicked(_ sender: Any) {
        self.activityIndicator.startAnimating()
        filtering()
        
        
    }
    
    @IBAction func date_button_clicked(_ sender: Any) {
        
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 7)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        picker.delegate = self

        picker.todayButtonTitle = "오늘"
        picker.is12HourFormat = true
        picker.locale = Locale(identifier: "ko_KR")
        picker.timeZone = TimeZone(abbreviation: "KST")!
        picker.dateFormat = "M월 d일"
        picker.includesMonth = true
        picker.highlightColor = UIColor(red: 153/255, green: 204/255, blue: 153/255, alpha: 1)
        picker.doneButtonTitle = "선택"
        picker.doneBackgroundColor = UIColor(red: 153/255, green: 204/255, blue: 153/255, alpha: 1)
        picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 20))
        picker.timeInterval = DateTimePicker.MinuteInterval.ten
        picker.isDatePickerOnly = true
        
        if #available(iOS 13.0, *) {
            picker.normalColor = UIColor.secondarySystemGroupedBackground
            picker.darkColor = UIColor.label
            picker.contentViewBackgroundColor = UIColor.systemBackground
            picker.daysBackgroundColor = UIColor.groupTableViewBackground
            picker.titleBackgroundColor = UIColor.secondarySystemGroupedBackground
        } else {
            picker.normalColor = UIColor.white
            picker.darkColor = UIColor.black
            picker.contentViewBackgroundColor = UIColor.white
        }
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.appointed_at = formatter.string(from: date)

            //print(self.appointed_at)
        }
        
        
        // add picker to your view
        // don't try to make customize width and height of the picker,
        // you'll end up with corrupted looking UI
//        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        // set a dismissHandler if necessary
//        picker.dismissHandler = {
//            picker.removeFromSuperview()
//        }
//        self.view.addSubview(picker)
        
        // or show it like a modal
        picker.show()
        
    }
    
    //Filter Layout
    @IBAction func filterParentLayoutClicked(_ sender: Any) {
        
        filterParentLayout.isHidden = true
        //show border
        self.tabBarController?.tabBar.clipsToBounds = false
        //show tabbar
        self.tabBarController?.tabBar.isHidden = false
        
    }
}
