//
//  HomeRoomCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/02/01.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class HomeRoomCell: UITableViewCell{
    
    
    
    
    @IBOutlet weak var introImage: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var exp: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var genderMember: UILabel!
    
    @IBOutlet weak var separatorLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.introImage.layer.cornerRadius = 5
        
        // 구분선 색깔
        separatorLine.backgroundColor = UIColor(displayP3Red: 188/255, green: 222/255, blue: 185/255, alpha: 1.0)
        exp.textColor = UIColor(displayP3Red: 84/255, green: 94/255, blue: 84/255, alpha: 1.0)
        
        date.font = UIFont(name: "NotoSansCJKkr-Regular", size: 11)
        genderMember.font = UIFont(name: "NotoSansCJKkr-Regular", size: 11)
        exp.font = UIFont(name: "NotoSansCJKkr-Regular", size: 12)
        
        
        //title.adjustsFontSizeToFitWidth = true
        //exp.adjustsFontSizeToFitWidth = true // exp에 fit width를 넣으면 글자가 들쑥날쑥해짐
        date.adjustsFontSizeToFitWidth = true
        genderMember.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    public func configureCell(){
        
        
    }
    

    
}
