//
//  IntroduceViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/12/30.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class IntroduceViewController: UIViewController {
    

    @IBOutlet weak var textView: UITextView!
    
    let defaultMessage = "내가 좋아하는 것 3가지는?"
    @IBOutlet weak var sendLabel: UIButton!
    
    @IBOutlet weak var countLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        debugLabels()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // tab bar 가리기
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    @IBAction func sendAction(_ sender: Any) {
        
        if textView.text == defaultMessage || textView.text == ""{
            OKDialog("빈 칸을 채워주세요 :)")
            return
        }
        
        if textView.text!.count < 5{
            OKDialog("조금만 더....")
            return
        }
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let uuid = selfuser[0].uuid
        
        let text: String = textView.text
        
        let body: Parameters = [
            "uuid": uuid,
            "introduce": text
        ]

        let url = dbInfo.secure_url + "api/query/changeMyIntroduce"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            
            switch result{
            case 101:
                let email = selfuser[0].email
                // email키값으로, email 기준으로 DB를 찾아서 해당 DB를 수정한다.
                try! realm.write {
                    realm.create(SelfUser.self, value: ["email": email, "introduce" : text], update: .modified)
                }
                
                self.sendLabel.isEnabled = false
                self.OKDialog("수정 했어요 :)")
                self.textView.isEditable = false
            default:
                self.OKDialog("알 수 없는 에러")
            }
        }
        
        
        
        
        
        
    }

    
    func debugLabels(){
        
        placeholderSetting()
        
        //로그인 버튼 셋팅
        sendLabel.layer.cornerRadius = 20.0
        //sendLabel.backgroundColor = UIColor(red: 255/255, green: 132/255, blue: 102/255, alpha: 1)
        sendLabel.tintColor = UIColor.black
        sendLabel.layer.borderWidth = 1
        sendLabel.layer.borderColor = UIColor.black.cgColor
        
        // textView 둥글게
        textView.layer.cornerRadius = 5
        
        textView.font = UIFont(name: "NotoSansCJKkr-Light", size: 18)
        
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        
        
        // 뒤로가기 버튼
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
        loadMyIntroduce()
        

        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    

    
    
    func loadMyIntroduce(){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let introduce = selfuser[0].introduce
        
        if introduce != ""{
        textView.text = introduce
        textView.textColor = UIColor(displayP3Red: 112/255, green: 112/255, blue: 112/255, alpha: 1.0)
            countLabel.text = String(textView.text.count)+"/200"
        }
    }
    
    
    
    func placeholderSetting() {
        textView.delegate = self // txtvReview가 유저가 선언한 outlet
        textView.text = defaultMessage
        textView.textColor = UIColor.lightGray
          
      }
      
      




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IntroduceViewController: UITextViewDelegate{
    
    @IBAction func ContinueClicked(_ sender: Any) {
        
        textView.text += "\n"
    }
    
    // TextView Place Holder
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            if textView.text == defaultMessage{
                textView.text = nil
            }
            textView.textColor = UIColor.black
        }
        
    }
    // TextView Place Holder
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = defaultMessage
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //limit
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        
        
        // \n 엔터키 3번이상 못누르게 하기
        if numberOfChars > 4{ if newText[numberOfChars - 1] == "\n" && newText[numberOfChars - 2] == "\n" && newText[numberOfChars - 3] == "\n"{ return false }}
        
        
        return numberOfChars <= 200    // 200 Limit Value
        
        
        
    }
    
    //explain - watcher
    func textViewDidChange(_ textView: UITextView) { //Handle
        
        //watcher
        countLabel.text = String(textView.text.count)+"/200"
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.endEditing(true)
        
    }
}
