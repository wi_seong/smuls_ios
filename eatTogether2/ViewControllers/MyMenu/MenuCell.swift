//
//  MenuCell.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/08/09.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class MenuCell:UICollectionViewCell{

    @IBOutlet weak var settingIcon: UIImageView!
    
    @IBOutlet weak var settingTag: UILabel!
    
}
