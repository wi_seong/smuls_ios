//
//  IAPHelpers.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/02/13.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import StoreKit
import UIKit
import Alamofire
import RealmSwift

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> Void

protocol IAPHelperDelegate: class {
    func CompletePurchase(transaction: SKPaymentTransaction)
    func Finished(transaction: SKPaymentTransaction)
}

extension Notification.Name {
    static let IAPHelperPurchaseNotification = Notification.Name("IAPHelperPurchaseNotification")
}

open class IAPHelper: NSObject  {
    private let productIdentifiers: Set<ProductIdentifier>
    private var purchasedProductIdentifiers: Set<ProductIdentifier> = []
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    
    private var purchasedTime = 0
    
    weak var delegate: IAPHelperDelegate?
    
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds

        /*
        for productIdentifier in productIds {
            let purchased = UserDefaults.standard.bool(forKey: productIdentifier)

            if purchased {
                purchasedProductIdentifiers.insert(productIdentifier)
                print("Previously purchased: \(productIdentifier)")
            } else {
                print("Not purchased: \(productIdentifier)")
            }
        }*/

        super.init()
        SKPaymentQueue.default().add(self)
        
    }
    
    deinit{
        // 메모리 해제 시
        productsRequest?.cancel()
    }
}

extension IAPHelper {
    public func requestProducts(_ completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    public func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

extension IAPHelper: SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        let products = response.products
        
        
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
        
        productsRequestCompletionHandler?(true, products)
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }
    
    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

extension IAPHelper: SKPaymentTransactionObserver {
    
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                
                if purchasedTime == 0{
                    purchasedTime += 1
                    complete(transaction: transaction)
                }
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                if purchasedTime == 0{
                    purchasedTime += 1
                    restore(transaction: transaction)
                }
                break
            case .deferred:
                break
            case .purchasing:
                break
            @unknown default:
                fatalError()
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        
        
        /*
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)*/
        //print("IAHelper 트랜젝션종료: ")
        
        //SKPaymentQueue.default().finishTransaction(transaction)
        
        // 종료 신호주기
        delegate?.CompletePurchase(transaction: transaction)
        
        
        
        
        
        
    }
    
    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        
        

        print("restore... \(productIdentifier)")
        /*
        deliverPurchaseNotificationFor(identifier: productIdentifier)*/
        
        // 종료 신호주기
        delegate?.CompletePurchase(transaction: transaction)
        
        //SKPaymentQueue.default().finishTransaction(transaction)
        
        
        
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        
        /*
        if let transactionError = transaction.error as NSError?,
            let localizedDescription = transaction.error?.localizedDescription,
            transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }*/
        
        // 종료 신호주기
        delegate?.Finished(transaction: transaction)
        
        //SKPaymentQueue.default().finishTransaction(transaction)
        
        
    
    }
    
    private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let identifier = identifier else { return }

        purchasedProductIdentifiers.insert(identifier)
        UserDefaults.standard.set(true, forKey: identifier)
        NotificationCenter.default.post(name: .IAPHelperPurchaseNotification, object: identifier)
    }
    
    
}
