//
//  StoreViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/11.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire
import RealmSwift
import SwiftyStoreKit

class StoreViewController: UIViewController{


    
    
    
    //var everyThingLoaded = false
    private static var handlerCount = 0 // 전역함수 선언
    
    let loadingTextLabel = UILabel()
    
    var store: IAPHelper?
    
    
    
    // 뒤로가기 버튼
    let lbutton = UIButton(type: UIButton.ButtonType.custom)
    
    //lazy private var purchaseIndicator : SYActivityIndicatorView = {
   //    let image = UIImage(named: "loading.png")
   //     return SYActivityIndicatorView(image: nil)
    //}()
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = CGPoint(x: view.frame.size.width  / 2,
                                           y: view.frame.size.height / 2 - 50)
        activityIndicator.hidesWhenStopped = false
        activityIndicator.style = .large
    
        return activityIndicator
        
    }()
    
    //let SerialQueue = DispatchQueue(label: "Purchase", qos: .background, attributes: [], autoreleaseFrequency: .workItem, target: nil)
    
    
    //@IBOutlet weak var productLabel1: UILabel!
    //@IBOutlet weak var productLabel2: UILabel!
    //@IBOutlet weak var productLabel3: UILabel!
    //@IBOutlet weak var price1: UILabel!
    //@IBOutlet weak var price2: UILabel!
    //@IBOutlet weak var price3: UILabel!
    
    @IBOutlet weak var purchaseLabel: UIButton!
    
    @IBOutlet weak var explainLabel: UILabel!
    
    @IBOutlet weak var detailExplainLabel: UILabel!
    @IBOutlet weak var detailView: UIView!
    //@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /*
    var defaultQueue = SKPaymentQueue()
    //var product = SKProduct()
    var products = [SKProduct]()
    var requiredProduct = 0
    var productIndex = [String: Int]()
    
    var purchased = [SKPaymentTransaction]()
    */

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 결제 indicator
        //self.view.addSubview(purchaseIndicator)
        //purchaseIndicator.center = self.view.center
        
        //결제모듈초기화 및 정보불러오기 -> 심사 3(2021년2월13일): thread crash
        //initPurchase()
        
        // 라벨 디버그
        debugLabels()

        
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // tab bar 숨기기
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // tab bar 다시 나타내기
        self.tabBarController?.tabBar.isHidden = false
    }
 
    
    private func debugLabels(){
        
        // activity indicator
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.hidesWhenStopped = true
        
        //self.activityIndicator.startAnimating()
        //productLabel1.text = "2500 코인"
        //price1.text = "2,500원"
        
        //productLabel2.text = "11000 코인"
        //price2.text = "11,000원"
        
        //productLabel3.text = "33000 코인"
        //price3.text = "30,000원"
        //purchaseLabel.layer.borderWidth = 1
        //purchaseLabel.layer.borderColor = UIColor.black.cgColor
        //purchaseLabel.layer.cornerRadius = 25
        
        explainLabel.text = "보증금은 2500 단위로 충전이 가능해요. \n충전하기를 누르면 앱 스토어에서 자동 결제 됩니다."
        

        detailView.layer.cornerRadius = 20
        detailExplainLabel.text = "<보증금 사용법>\n\n클럽을 만들거나 참여하기 위해서는\n보증금 1500원이 필요합니다.\n\n- 약속 시간에 출석하면 전액 환급\n- 결석 0원, 지각 1000원 환급"
        
        loadingTextLabel.textColor = UIColor.black
        loadingTextLabel.text = "결제가 진행중입니다. 다른화면으로 이동하지 마십시오"
        loadingTextLabel.textAlignment = .center
        loadingTextLabel.numberOfLines = 0
        loadingTextLabel.font = UIFont(name: "Avenir Light", size: 14)
        loadingTextLabel.sizeToFit()
        loadingTextLabel.center = CGPoint(x: view.frame.size.width  / 2,
                                          y: view.frame.size.height / 2 - 20)
            view.addSubview(loadingTextLabel)
        self.loadingTextLabel.isHidden = true
        
        
        
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func initPurchase(){
        
        // thread crash 문제로 결제 시, 한꺼번에 가져오기
        //indicator start

        //self.activityIndicator.startAnimating()
        
        
        //let ConcurrentQueue = DispatchQueue(label: "initPurchase", qos: .userInitiated, attributes: [.concurrent], autoreleaseFrequency: .workItem, target: nil)
        
        
        //ConcurrentQueue.sync {
            
            // 비동기로 불러온다. 비동기로 안 불러오고, 사용자가 초기화 중에
            // 버튼을 누르게 되면 동작이 연달아서 되어 불편을 끼치게 됨.
            //let productID: NSSet = NSSet(objects: "smuls_product_2500", "smuls_product_11000", "smuls_product_30000")
            
            
        //}

        
        
    }
    @IBAction func FirstProductBtnAction(_ sender: Any) {
        
        // 정지된 회원은 충전하지 못하도록
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        if selfuser[0].is_blocked{
            self.OKDialog("계정이 정지되었습니다.\n관리자에게 문의바랍니다.")
            return
        }
        
        FirstProductButton()
    }
    
    private func FirstProductButton(){
        
        //indicator start
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.loadingTextLabel.text = "결제가 진행중입니다. 다른화면으로 이동하지 마십시오"
            self.loadingTextLabel.isHidden = false
            
            // 결제 중 버튼 비활성화
            self.purchaseLabel.isEnabled = false
            
            // 뒤로가기 막기
            // 네비게이션 팝제스처 임시 해제
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            //뒤로가는 버튼 임시 해제
            self.lbutton.isEnabled = false
            
        }
        
        // 결제 후 completehandelr가 여러번 호출해서 서버에 여러번 결제 메세지를 날리지 않기 위해
        StoreViewController.handlerCount = 0 // 카운트 초기화
        

        
        SwiftyStoreKit.purchaseProduct("smuls_product_2500", quantity: 1, atomically: false) { result in
            switch result {
            case .success(let product):
                // fetch content from your server, then:
                self.handlePurchased(product.transaction as! SKPaymentTransaction){
                    
                    // handlerPurchase 함수 호출 후 다음 함수 실행
                    if product.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(product.transaction)
                    }
                    print("Purchase Success: \(product.productId)")
                    
                }
                
                
            case .error(let error):
                self.Cancelled() // UI 원위치
                
                switch error.code {
                case .unknown: self.OKDialog("Unknown error. Please contact support")
                case .clientInvalid: self.OKDialog("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: self.OKDialog("The purchase identifier was invalid")
                case .paymentNotAllowed: self.OKDialog("The device is not allowed to make the payment")
                case .storeProductNotAvailable: self.OKDialog("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: self.OKDialog("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: self.OKDialog("Could not connect to the network")
                case .cloudServiceRevoked: self.OKDialog("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
        
        
        
    }
    
    private func Cancelled() {
        DispatchQueue.main.async {
            //indicator stop
            self.activityIndicator.stopAnimating()
            self.loadingTextLabel.isHidden = true
            
            // 버튼 활성화
            self.purchaseLabel.isEnabled = true
            
            // 뒤로가기 원위치
            // 네비게이션 팝제스처 활성화
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            //뒤로가는 버튼 활성화
            self.lbutton.isEnabled = true
        }
    }

    private func handlePurchased(_ transaction: SKPaymentTransaction, handler: @escaping () -> Void){
        
        DispatchQueue.main.async {
            self.loadingTextLabel.text = "작업을 완료하고 있습니다. 잠시만 기다려주십시오."
        }
        
        // 결제 후 completehandler가 여러번 호출해서 서버에 여러번 결제 메세지를 날리지 않기 위해
        if StoreViewController.handlerCount == 0{
            StoreViewController.handlerCount += 1
        }
        else{
            print("다수의 handler호출")
            return
        }
    
        
        //print("영수증 주소 : \(String(describing: Bundle.main.appStoreReceiptURL))")
        
        let receiptData = NSData(contentsOf: Bundle.main.appStoreReceiptURL!)
        print(receiptData!)
        
        let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions())
        
        print("구매 성공 트랜젝션 아이디: \(String(describing: transaction.transactionIdentifier))")
        print("상품 아이디: \(transaction.payment.productIdentifier)")
        //print("구매 영수증: \(String(describing: receiptString))")
        
        
        
        
        // 서버에 전송하기
        // ----- ********************************** ------ //
        
        //이메일 불러오기
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        let email = selfuser[0].email
        
        let body: Parameters = [
            "email": email,
            "product_id": transaction.payment.productIdentifier,
            "receipt": receiptString!,
            "transaction_id": transaction.transactionIdentifier!
        ]

        let url = dbInfo.secure_url + "api/purchase/ios_userPurchase"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            //activity indicator stop
            DispatchQueue.main.async {
                //indicator stop
                self.activityIndicator.stopAnimating()
                self.loadingTextLabel.isHidden = true
                
                // 버튼 활성화
                self.purchaseLabel.isEnabled = true
                
                // 뒤로가기 원위치
                // 네비게이션 팝제스처 활성화
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                //뒤로가는 버튼 활성화
                self.lbutton.isEnabled = true
            }
            
            switch result{
            case 101:
                self.OKDialog("구매가 정상적으로 완료되었습니다.")
                break
            case 102:
                self.OKDialog("결제한 제품과 인증제품이 다릅니다.")
                break
            case 111:
                self.OKDialog("결제 중 에러")
                break
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
            
            // escaping fuction
            handler()
        
        
        }
        
        
        
        
        // -------------------------------------------------
        
    }
    
    

    


    
    
    
    
    /*----------------------------------*/
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

