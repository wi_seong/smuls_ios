//
//  VerifyGenderViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/21.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import NaverThirdPartyLogin
import Alamofire
import RealmSwift


class VerifyGenderViewController: UIViewController {
    
    //네이버 로그인 인스턴스
    let loginInstance = NaverThirdPartyLoginConnection.getSharedInstance()
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var loginButtonLabel: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // delegate 할당
        loginInstance?.delegate = self
        
        setupLabels()
        
    }
    
    //Login Buttonsp
    @IBAction func LoginAction(_ sender: Any) {
        
        loginInstance?.requestThirdPartyLogin()
        
    }
    
    

    private func modifyGenderName(_ gender: String) -> String{
        if gender == "M" { return "남성" }
        else if gender == "F" { return "여성" }
        
        else { return "미인증" }
    }
    
    //functions
    private func setupLabels(){
        //로그인 버튼 셋팅
    
        
        nameLabel.isHidden = true
        
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        let gender = selfuser[0].gender
        
        let tranformed_gender = modifyGenderName(gender)
        print(tranformed_gender)
        if tranformed_gender != "미인증"{
            statusLabel.text = "성별 인증 : " + tranformed_gender
            nameLabel.text = "성함 : " + selfuser[0].name
            nameLabel.isHidden = false
            loginButtonLabel.isEnabled = false
        }
    }
    
    private func getNaverInfo(){

        guard let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() else { return }
         
         if !isValidAccessToken {
            OKDialog("에러: 유효하지 않은 토큰 201")
           return
         }
         
         guard let tokenType = loginInstance?.tokenType else { return }
         guard let accessToken = loginInstance?.accessToken else { return }
         let urlStr = "https://openapi.naver.com/v1/nid/me"
         let url = URL(string: urlStr)!
         
         let authorization = "\(tokenType) \(accessToken)"
         
         let req = AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": authorization])
         
         req.responseJSON { response in
            guard let result = response.value as? [String: Any] else { return }
            guard let object = result["response"] as? [String: Any] else { return }
            guard let name = object["name"] as? String else { return }
            guard let gender = object["gender"] as? String else { return }
            guard let id = object["id"] as? String else { return }
            
            
           
            //마지막으로 로그아웃시켜주기
            self.loginInstance?.requestDeleteToken()
            
            //서버에 업로드 하기
            self.uploadToServer(name, gender, id)
         }
        
        
        
    }
    
    func uploadToServer(_ name: String, _ gender: String, _ id: String){
        
        //이메일 불러오기
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        let email = selfuser[0].email

         let body: Parameters = [
             "email": email,
             "name": name,
             "gender": gender,
             "id": id
         ]

        let url = dbInfo.secure_url + "api/auth/upload_gender"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in
  
            let result = json["result"].intValue
            
            //print(result)
            switch result{
            case 101:
                self.OKDialog("성별이 인증되었습니다")
                self.updateLabelsAndRealm(Int(id)!, name, gender)
                
            case 111:
                self.OKDialog("업데이트에 실패했습니다.\n관리자에게 문의해주세요.")
            default:
                self.OKDialog("알 수 없는 에러")
            }
            
            
            
        }
        
        
        
    }
    
    func updateLabelsAndRealm(_ id: Int, _ name: String, _ gender: String){
        
        // 라벨 바꾸기
        statusLabel.text = "성별 인증 : " + modifyGenderName(gender)
        nameLabel.text = "성함 : " + name
        nameLabel.isHidden = false
        
        loginButtonLabel.isEnabled = false
        
        
        // Ream DB 업데이트
        
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        let email = selfuser[0].email

        // 부분적으로 업데이트 하기 (email키값을 찾는다)
        try! realm.write {
            realm.create(SelfUser.self, value: ["email": email, "gender": gender, "name": name, "id": id], update: .modified)
       
        }
        
    }
    
    
    
    
    
}

extension VerifyGenderViewController: NaverThirdPartyLoginConnectionDelegate{
    
    //naver login delegates
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        
        print("[Success] : Success Naver Login")
        getNaverInfo()
        
    }
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        print("[Success] : Success Naver Login")
        getNaverInfo()
        
    }
    func oauth20ConnectionDidFinishDeleteToken() {
        loginInstance?.requestDeleteToken()
    }
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        //print("[Error] :", error.localizedDescription)
        OKDialog("에러: \(error.localizedDescription)")
    }
}
