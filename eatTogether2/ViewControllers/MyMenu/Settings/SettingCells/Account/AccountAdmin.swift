//
//  AccountAdmin.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/12/08.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift


class AccountAdmin: UITableViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let numberOfSections = 1
    let row = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    
    
    private func sendPWFmail(){
        
        activityIndicator.startAnimating()

        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
    
        let email = selfuser[0].email
 
        let body: Parameters = [
            "email": email
        ]

        let url = dbInfo.secure_url + "pwf"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            self.activityIndicator.stopAnimating()
            
            switch result{
            case 101:
                
                self.OKDialog("비밀번호 변경 이메일 발송되었습니다.\n이메일을 확인해주세요.\n(스팸메일함으로 전송될 수 있습니다.)")
               
                break
            case 201:
                self.OKDialog("인증메일을 연속으로 보낼 수 없습니다.\n잠시 후 시도해주세요.")
            case 401:
                self.OKDialog("이메일 정보가 없습니다. 다시 시도해주세요.")
                
                break
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
        }
        
    }
    // MARK: - Table view data source
    
    
    // 클릭했을 때
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // 비밀번호 찾기 눌렀을 때
        if indexPath.section == 0 && indexPath.row == 0{

            sendPWFmail()
        }
    }
    
    
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return row
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
