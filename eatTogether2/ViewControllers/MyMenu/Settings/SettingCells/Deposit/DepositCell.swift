//
//  DepositCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/02/01.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class DepositCell: UITableViewCell{
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var stateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    public func configureCell(depo: depositModel){
        
        var val = ""
        var state = ""

        switch depo.type {
        case 0:
            val = String(depo.value) + "원 충전"
            state = "무료 보증금 충전"
            break
            
        case 1:
            val = String(depo.value) + "원 사용"
            state = "클럽 생성"
            break
            
        case 2:
            val = String(depo.value) + "원 사용"
            state = "클럽 참여"
            break
            
        case 3:
            val = String(depo.value) + "원 환급"
            state = "클럽 출석 인정!"
            break
        case 4:
            val = String(depo.value) + "원 환급"
            state = "클럽 지각"
            break
            
        case 5:
            val = String(depo.value) + "원 차감"
            state = "클럽 출석 이의제기"
            break
            
        case 6:
            val = String(depo.value) + "원 결제"
            state = "구입"
            break
        
        case 7:
            val = String(depo.value) + "원 충전"
            state = "운영팀이 보냈습니다."
            break
            
        default:
            val = String(depo.value)
            state = ""
            break
        }
        
        balanceLabel.text = String(depo.balance) + "원"
        
        contentLabel.text = depo.content
        valueLabel.text = val
        stateLabel.text = state
        
        dateLabel.text = depo.created_at
        

    }
    
}
