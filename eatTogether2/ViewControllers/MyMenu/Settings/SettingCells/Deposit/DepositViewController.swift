//
//  DepositViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/02/01.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class DepositViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var deposit = [depositModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.title = "보증금 내역"

        // Do any additional setup after loading the view.
        
        
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        // 더미데이터 넣기
        //setDummyData()
        
        getDepositList()
    }
    
    private func getDepositList(){
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        let uuid = selfuser[0].uuid
        
        let url = dbInfo.secure_url + "api/query/inquireMyDepositIO"
        
        let body = ["uuid": uuid]
            
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in
            self.deposit.removeAll()
            let array = json["depositIO"].array!
             
            for j in array{
                let temp = depositModel(j)
                self.deposit.append(temp)
            }
            self.deposit.reverse()
            self.tableView.reloadData()
        }
    }
    
    
    
    
    private func setDummyData(){
        
        let json0: JSON = [
            "content": "",
            "type": 0,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json0))
        
        let json1: JSON = [
            "content": "스키타러가자",
            "type": 1,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json1))
        
        let json2: JSON = [
            "content": "스키타러가자",
            "type": 2,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json2))
        
        let json3: JSON = [
            "content": "스키타러가자",
            "type": 3,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json3))
        
        let json4: JSON = [
            "content": "스키타러가자dddd",
            "type": 4,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json4))
        
        let json5: JSON = [
            "content": "",
            "type": 5,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json5))
        
        let json6: JSON = [
            "content": "",
            "type": 6,
            "value": 1500,
            "balance": 5000,
            "created_at": "2021-02-05T04:40:16.000Z"
        ]
        deposit.append(depositModel(json6))
        
        deposit.reverse()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DepositViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return deposit.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        var cell: DepositCell
        cell = tableView.dequeueReusableCell(withIdentifier: "DepositCell") as! DepositCell
        cell.configureCell(depo: deposit[indexPath.row])
        return cell
            
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: false)
        
    }
}
