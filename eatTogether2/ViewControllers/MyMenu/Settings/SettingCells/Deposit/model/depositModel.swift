//
//  depositModel.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/02/01.
//  Copyright © 2021 alomteamd. All rights reserved.
//
import Foundation
import SwiftyJSON


class depositModel: CustomStringConvertible{
    
    var content: String
    var type: Int
    var value: Int
    var balance: Int
    var created_at: String
    
    
    
    init(_ data: JSON){
    
        self.content = data["content"].stringValue
        self.type = data["type"].intValue
        self.value = data["value"].intValue
        self.balance = data["balance"].intValue
        
        let time = data["created_at"].stringValue
        
        // 시간 계산
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        let date: Date = dateFormatter.date(from: time)!
        
        let changedDate = DateFormatter()
        
        
        changedDate.locale = Locale(identifier: "ko_KR")
        changedDate.timeZone = TimeZone(abbreviation: "KST")
        
        changedDate.dateFormat = "M월d일 H:mm"
        let dateTime = changedDate.string(from: date)

        self.created_at = dateTime

    }
    
    
    var description: String{
        return "content: \(self.content) type: \(self.type) value: \(self.value) balance: \(self.balance) created_at: \(self.created_at)"
    }
    
    
    
    
}
