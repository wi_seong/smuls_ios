//
//  PersonInfoViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/12/07.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import WebKit

class PersonInfoViewController: UIViewController, WKUIDelegate, WKNavigationDelegate{

    

    
    @IBOutlet weak var webView: WKWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        loadURL()
    }
    

    func loadURL(){
        view.addSubview(webView)
        
        let url = URL(string: "https://smuls.com/PrivacyPolicy.html")!
        let request = URLRequest(url: url)
        
        webView.load(request)
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.title = "개인정보 처리방침"
    }
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
