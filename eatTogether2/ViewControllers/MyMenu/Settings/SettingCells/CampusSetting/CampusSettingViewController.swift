//
//  CampusSettingViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/28.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class CampusSettingViewController: UIViewController {

    
    let realm = try! Realm()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var campusNames = Array<String>() // 캠퍼스 이름들
    
    var campus_num = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //캠퍼스이름 초기화
        initCampus()
        
        
    }
    
    
    func initCampus(){

        //캠퍼스 여러개중에서 내가 현재 속한 캠퍼스 찾기
        let selfuser = realm.objects(SelfUser.self)
        campus_num = selfuser[0].campus_type
        

        let campusArray = realm.objects(campusDTO.self)
        
        
        // 로컬 DB에 저장된 캠퍼스 정보를 화면에 tableView로 띄워준다.
        
        
        for i in 0..<campusArray.count{
            
            campusNames.append(campusArray[i].name)
            tableView.reloadData()
            
        }
 
    
        
    }
    
    func selectedCampus(index: Int){
        
        
        // indicator start
        activityIndicator.startAnimating()
        
        //서버에 주 캠퍼스 바꼈다고 통보해주기
        
        //Realm에 user정보 수정
        let selfuser = realm.objects(SelfUser.self)
        let email = selfuser[0].email
        
        
        // email키값으로, email 기준으로 DB를 찾아서 해당 DB를 수정한다.
        try! realm.write {
            realm.create(SelfUser.self, value: ["email": email, "campus_type" : index + 1], update: .modified)
       
        }
        let type = index + 1
        let body: Parameters = [
            "email": email,
            "type": type
        ]

        // 캠퍼스 정보 서버에 업데이트하기, 내가 선택한 캠퍼스 업데이트
        let url = dbInfo.secure_url + "api/query/changeMyCampus"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            self.activityIndicator.stopAnimating()
            
            switch result{
            case 101:
                // 홈화면에 새로고침 flag 표시해주기
                sessionInfo.isRequiredToRefreshHome = true
                
                // 성공했으니 이전화면으로 돌아가기
                self.navigationController?.popViewController(animated: true)
                break
 
            default:
                self.OKDialog("알 수 없는 에러")
            }
        }
        
        
        
        
        
    }
    
    

    
}

extension CampusSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return campusNames.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        //되돌아가기
        if indexPath.row != campus_num - 1{
            selectedCampus(index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell: CampusSettingCell

        print("생성된 배열인덱스: \(indexPath.row) \(campus_num - 1)")
        cell = tableView.dequeueReusableCell(withIdentifier: "CampusCell") as! CampusSettingCell
        if indexPath.row == campus_num - 1{
            cell.configure(campusName: campusNames[indexPath.row], "YES")
        }
        else {
            cell.configure(campusName: campusNames[indexPath.row], "NO")
        }
        cell.selectionStyle = .none
        
        
        return cell

    }
    
}
