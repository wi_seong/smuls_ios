//
//  CampusSettingCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/28.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class CampusSettingCell: UITableViewCell{
    
    @IBOutlet weak var CampusName: UILabel!
    
    @IBOutlet weak var check: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: false)
        
        
    }
    
    func configure(campusName: String, _ isVisible: String){
        CampusName.text = campusName
        
        if isVisible == "YES"{
            check.isHidden = false
        }
        else if isVisible == "NO"{
            check.isHidden = true
        }
        //if isSelected{check.isHidden = false}
        //else if isSelected == false{check.isHidden = true}
        
    }
    
}
