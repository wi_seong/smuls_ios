//
//  SettingCellTableView.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/14.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift


class SettingCellTableView: UITableViewController{
    
    
    @IBOutlet weak var naverAccountStatus: UILabel!
    @IBOutlet weak var GenderStatus: UILabel!
    @IBOutlet weak var campusNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tab bar 숨기기
        self.tabBarController?.tabBar.isHidden = true
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        // 라벨 업데이트
        debugLabels()
        
    }
    
    
    //functions 함수들
    
    private func debugLabels(){
        
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        
        //성별 및 네이버 계정 인증
        let gender = selfuser[0].gender
        
        if gender == "M"{
            GenderStatus.text = "남성"
            //naverAccountStatus.text = "인증완료"
        }
        else if gender == "F"{
            GenderStatus.text = "여성"
            //naverAccountStatus.text = "인증완료"
        }
        
        let campus_num = selfuser[0].campus_type

        
        //캠퍼스 여러개중에서 내가 현재 속한 캠퍼스 찾기
        let campus_info = realm.objects(campusDTO.self).filter("num == %@", campus_num)
        
        let campus_name = campus_info[0].name
        
        campusNameLabel.text = campus_name
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //print("The selected setion is: \(indexPath.section)")
        /*
        if indexPath.section == 4 && indexPath.row == 0{ // 고객센터 선택 시, 카카오플러스친구로 자동 연결 
            if let url = URL(string: "http://pf.kakao.com/_LVjnK/chat") {
                                                    UIApplication.shared.open(url, options: [:])
            }
            
        }*/
    }
}
