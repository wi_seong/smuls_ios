//
//  MyViewController.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/26.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire



class MyViewController: UIViewController, UIGestureRecognizerDelegate{
    
    //widget
    
    @IBOutlet weak var option1: UIStackView!
    
    @IBOutlet weak var option2_layout: UIButton!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var option3: UIStackView!
    
    @IBOutlet weak var option4: UIImageView!
    
    @IBOutlet weak var NicknameLabel: UILabel!
    
    @IBOutlet weak var DepositLabel: UILabel!
    
    var settingData = ["스토어","설정","내 소개"]


    

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // 스와이프로 뒤로가기 허가
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        
        debugLabels()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // 새로고침해야할게 있다면 고쳐주기
        updateMyCoin()
        
        // tab bar 보이기(애니메이션)
        tabBarController?.tabBar.isHidden = false
        UIView.transition(with: tabBarController!.view, duration: 0.2, options: .transitionCrossDissolve, animations: nil)
        
        // 네비게이션 팝제스처 추가
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // Functions
    
    private func debugLabels(){
    
        //database
        let realm = try! Realm()
        let selfuser = realm.objects(SelfUser.self)
        
        //성별 및 네이버 계정 인증
        let email = selfuser[0].email
        let NickName = selfuser[0].nick_name
        let coin = selfuser[0].coin
        
        NicknameLabel.text = NickName
        emailLabel.text = email
        DepositLabel.text = "보증금:   " + String(coin) + "원"
        
        //layout
        option2_layout.layer.cornerRadius = 15
        option2_layout.layer.borderWidth = 1
        option2_layout.layer.borderColor = UIColor.black.cgColor
        
        //onclick
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(option1_clicked(tapGestureRecognizer:)))
            option1.isUserInteractionEnabled = true
        option1.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(option3_clicked(tapGestureRecognizer:)))
        option3.isUserInteractionEnabled = true
        option3.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(option4_clicked(tapGestureRecognizer:)))
        option4.isUserInteractionEnabled = true
        option4.addGestureRecognizer(tapGestureRecognizer3)
        
        
        // Tabbar 라인 없애기
        //UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().layer.borderWidth = 0
        
        // 배경색
        view.backgroundColor = UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        
    }
    
    // !-----------------------------------------------------!
    // When buttons clicked
    
    //Charge button clicked
    
    //introduce
    @objc func option1_clicked(tapGestureRecognizer: UITapGestureRecognizer){
        
        DispatchQueue.main.async {
            let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroduceView")
            self.navigationController?.pushViewController(pushVC!, animated: true)
        }
        
    }
    
    //store
    @IBAction func option2(_ sender: Any) {
        
        DispatchQueue.main.async {
            let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "StoreViewController")
            self.navigationController?.pushViewController(pushVC!, animated: true)
        }
        
    }
    
    //setting
    @objc func option3_clicked(tapGestureRecognizer: UITapGestureRecognizer) {
        
        DispatchQueue.main.async {
            let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingView")
            self.navigationController?.pushViewController(pushVC!, animated: true)
        }
        
    }
    
    //email
    @objc func option4_clicked(tapGestureRecognizer: UITapGestureRecognizer){
        if let url = URL(string: "http://pf.kakao.com/_LVjnK/chat") {
                                                UIApplication.shared.open(url, options: [:])
        }
        
    }

    
    func updateMyCoin(){
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let uuid = selfuser[0].uuid
        let email = selfuser[0].email
        
        
        let body: Parameters = [
            "uuid": uuid
        ]

        let url = dbInfo.secure_url + "api/query/getMyCoinAmount"
        Methods.shared.SecureAlamoPost( url: url, body: body){ (json) in  // SecureAlamoPost랑 AlamoPost 함수랑 다름. service_server사용할 때는 AlamoPost함수 이용하도록!
            
            let result = json["result"].intValue
            
            switch result{
            case 101:
                let defaultText = "보증금:   "
                let coin = json["coin"].stringValue
                let coinInt = json["coin"].intValue
                let sumText = defaultText + coin + "원"
                self.DepositLabel.text = sumText
                
                // email키값으로, email 기준으로 DB를 찾아서 해당 DB를 수정한다.
                try! realm.write {
                    realm.create(SelfUser.self, value: ["email": email, "coin" : coinInt], update: .modified)
                }

                break
            case 102:
                self.OKDialog("에러발생 301")
            default:
                self.OKDialog("알 수 없는 에러")
                break
            }
        }
    }
    
    
    
    
    // !-----------------------------------------------------!
    // When tapped cell
    
    /*
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      print("item at \(indexPath.section)/\(indexPath.item) tapped")
        
        //스토어 버튼을 눌렀을 떼
        if indexPath.row == myMenu.store.rawValue{
            DispatchQueue.main.async {
                let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "StoreView")
                self.navigationController?.pushViewController(pushVC!, animated: true)
            }
            
        }
        // 설정 버튼을 눌렀을 때
        else if indexPath.row == myMenu.settings.rawValue{
            
            DispatchQueue.main.async {
                let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingView")
                self.navigationController?.pushViewController(pushVC!, animated: true)
            }
            
        }
        
        //내 소개 버튼 눌렀을 때
        else if indexPath.row == myMenu.introduce.rawValue{
            DispatchQueue.main.async {
                let pushVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroduceView")
                self.navigationController?.pushViewController(pushVC!, animated: true)
            }
            
        }
    }
    
    //for cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width: (collectionView.bounds.width / 5), height: (collectionView.bounds.width / 5))
        return cellSize
    }
    
    //for margin of cell layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //handle data
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return settingData.count
    }
    
    
    
    
    // !-----------------------------------------------------!
    //push data, 데이터 초기화
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.settingTag.text = settingData[indexPath.row]
        
        // 인덱스 접근은 아래 enum rawValue로 접근할 것! 법용성, 확장성을 위해
        
        
        if indexPath.row == myMenu.store.rawValue{
            cell.settingIcon.image = UIImage(named:"mymenu_asset_my")
        } else if indexPath.row == myMenu.settings.rawValue{
            cell.settingIcon.image = UIImage(named:"mymenu_asset_setting")
        } else if indexPath.row == myMenu.introduce.rawValue{
            cell.settingIcon.image = UIImage(named: "mymenu_asset_my")
        }
        
        return cell
    }
     */
    
}


extension MyViewController{
    
    enum myMenu: Int {
        case store = 0
        case settings = 1
        case introduce = 2
    }
}
