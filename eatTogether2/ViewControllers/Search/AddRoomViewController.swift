//
//  AddRoomViewController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/16.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import CropViewController
import DateTimePicker
import AYPopupPickerView
import RealmSwift
import Alamofire

class AddRoomViewController: UIViewController, UITextViewDelegate, CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate{

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var imageViewButton: UIView!
    
    @IBOutlet weak var titleField: UITextField!
    
    @IBOutlet weak var genderLabel: UIButton!
    
    @IBOutlet weak var categoryLabel: UIButton!
    
    @IBOutlet weak var dateLabel: UIButton!
    
    @IBOutlet weak var peopleNum: UIButton!
    
    @IBOutlet weak var placeField: UITextField!
    
    @IBOutlet weak var introduceLabel: UITextView!
    
    @IBOutlet weak var introduceCountLabel: UILabel!
    
    @IBOutlet weak var imageBtnView: UIView!
    
    @IBOutlet weak var makeLabel: UIButton!
    
    @IBOutlet weak var ScrollView: UIScrollView!
    
    let placeHolder = "클럽을 소개해 주세요"
    
    
    var categoryData: [String] = ["Eat", "Play", "Learn", "etc"]
    
    //gender
    var GENDER = 0 //all
    
    var selectedCategory = -1
    var appointed_at = ""
    var gender_fix = "none"
    
    //add image
    var imagePicker = UIImagePickerController()
    var selectedImage:Data? = nil
    
    //member
    var MEMBER = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //레이아웃 설정
        debugLabels()
        introduceLabel.delegate = self
        
        
        titleField.delegate = self
        placeField.delegate = self

        
        
        //attach onclick to imageview
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(select(tapGestureRecognizer:)))
            imageViewButton.isUserInteractionEnabled = true
            imageViewButton.addGestureRecognizer(tapGestureRecognizer)

        // Do any additional setup after loading the view.
    }
    
    // 키보드 켜진 상태에서 화면 눌렀을 때
    
    private func debugLabels(){
        
        //imageView.layer.borderWidth = 2
        //imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.cornerRadius = 5
        
        //titleField.layer.borderWidth = 2
        //titleField.layer.borderColor = UIColor.black.cgColor
        titleField.layer.cornerRadius = 5
        
        placeField.layer.cornerRadius = 5
        
        genderLabel.layer.cornerRadius = 5
        
        categoryLabel.layer.cornerRadius = 5
        
        dateLabel.layer.cornerRadius = 5
        
        peopleNum.layer.cornerRadius = 5
        
        //introduceLabel.layer.borderColor = UIColor.black.cgColor
        //introduceLabel.layer.borderWidth = 2
        introduceLabel.layer.cornerRadius = 5
        
        introduceLabel.text = placeHolder
        introduceLabel.textColor = UIColor.lightGray
        introduceLabel.textContainerInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)

        
        makeLabel.layer.cornerRadius = 25
        makeLabel.layer.shadowColor = UIColor.black.cgColor
        makeLabel.layer.masksToBounds = false
        makeLabel.layer.shadowOffset = CGSize(width: 1, height: 4)
        makeLabel.layer.shadowRadius = 8
        makeLabel.layer.shadowOpacity = 0.3
        
        // 텍스트필드 여백 주기
        titleField.setLeftPaddingPoints(20)
        titleField.setRightPaddingPoints(20)
        placeField.setLeftPaddingPoints(20)
        placeField.setRightPaddingPoints(20)
        
        
        
        //키보드 숨김
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        tapGesture.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGesture)
        
        // 글 쓸 때 키보드 가려지는 것 방지
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func hideKeyBoard(){
        titleField.endEditing(true)
        introduceLabel.endEditing(true)
        placeField.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //introduceLabel.resignFirstResponder()
        introduceLabel.text += "\n"

        return true

    }

    @objc func keyboardWillShow(_ sender: Notification) {

        self.view.frame.origin.y = -300 // Move view 150 points upward
        self.view.layoutIfNeeded()

    }

    @objc func keyboardWillHide(_ sender: Notification) {

        self.view.frame.origin.y = 0 // Move view to original position
        self.view.layoutIfNeeded()

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Button Clicked
    
    @objc func select(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        let alert = UIAlertController(title: "사진 선택", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "갤러리", style: .default, handler: { _ in
            
            self.openGallary()
            
        }))
        
        alert.addAction(UIAlertAction(title: "이미지삭제", style: .default, handler: { _ in
            //이미지삭제
            self.imageView.image = nil
            self.selectedImage = nil
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "취소", style: .cancel, handler: { _ in
            self.imageBtnView.isHidden = false
            self.imageViewButton.isHidden = false
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func genderBtn(_ sender: Any) {
        
        if GENDER == 0 {
            GENDER = 1
            genderLabel.setTitle("women", for: .normal)
        }else if GENDER == 1{
            GENDER = 2
            genderLabel.setTitle("men", for: .normal)
        }else{
            GENDER = 0
            genderLabel.setTitle("all", for: .normal)
        }
        
    }
    
    
    @IBAction func categoryBtn(_ sender: Any) {
        titleField.resignFirstResponder()
        placeField.resignFirstResponder()
        introduceLabel.resignFirstResponder()
        
        let popupPickerView = AYPopupPickerView()
        
        popupPickerView.display(itemTitles: categoryData, doneHandler: { [self] in
            let selectedIndex = popupPickerView.pickerView.selectedRow(inComponent: 0)
            print("selected row: \(selectedIndex)")
            self.categoryLabel.setTitle(self.categoryData[selectedIndex], for: .normal)
            self.selectedCategory = selectedIndex
        })

        
    }
    

    @IBAction func dateBtn(_ sender: Any) {
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 7)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        picker.delegate = self

        picker.todayButtonTitle = "오늘"
        picker.is12HourFormat = true
        picker.locale = Locale(identifier: "ko_KR")
        picker.timeZone = TimeZone(abbreviation: "KST")!
        picker.dateFormat = "M월 d일 h:mm a"
        picker.includesMonth = true
        picker.highlightColor = UIColor(red: 153/255, green: 204/255, blue: 153/255, alpha: 1)
        picker.doneButtonTitle = "선택"
        picker.doneBackgroundColor = UIColor(red: 153/255, green: 204/255, blue: 153/255, alpha: 1)
        picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 20))
        picker.timeInterval = DateTimePicker.MinuteInterval.ten
        
        if #available(iOS 13.0, *) {
            picker.normalColor = UIColor.secondarySystemGroupedBackground
            picker.darkColor = UIColor.label
            picker.contentViewBackgroundColor = UIColor.systemBackground
            picker.daysBackgroundColor = UIColor.groupTableViewBackground
            picker.titleBackgroundColor = UIColor.secondarySystemGroupedBackground
        } else {
            picker.normalColor = UIColor.white
            picker.darkColor = UIColor.black
            picker.contentViewBackgroundColor = UIColor.white
        }
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.appointed_at = formatter.string(from: date)

            print(self.appointed_at)
        }
        
        
        // add picker to your view
        // don't try to make customize width and height of the picker,
        // you'll end up with corrupted looking UI
//        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        // set a dismissHandler if necessary
//        picker.dismissHandler = {
//            picker.removeFromSuperview()
//        }
//        self.view.addSubview(picker)
        
        // or show it like a modal
        picker.show()
        
    }

    func localeToUtc(localeDate: String, dateFormat: String) -> String
        {
            let dfFormat = DateFormatter()
            dfFormat.dateFormat = dateFormat
            dfFormat.timeZone = TimeZone.current
            let dtLocaleDate = dfFormat.date(from: localeDate)
            
            dfFormat.timeZone = TimeZone(abbreviation: "UTC")
            dfFormat.dateFormat = dateFormat
            return dfFormat.string(from: dtLocaleDate!)
        }
    
    
    @IBAction func plusBtn(_ sender: Any) {
        
        MEMBER = MEMBER + 1
        peopleNum.setTitle(String(MEMBER) + "명", for: .normal)
        
    }
    @IBAction func minusBtn(_ sender: Any) {
        
        if(MEMBER > 2){
            MEMBER = MEMBER - 1
            peopleNum.setTitle(String(MEMBER) + "명", for: .normal)
        }
        
    }
    
    //member end
    
    //unuse (too small)
    @IBAction func imageBtn(_ sender: Any) {
        
    }
    
    //
    
    @IBAction func makeBtn(_ sender: Any) {
        
        if selectedImage == nil{
            OKDialog("사진을 올려주세요")
            return
        }
        if titleField.text == ""{
            OKDialog("클럽 제목을 적어주세요")
            return
        }
        if selectedCategory == -1{
            OKDialog("카테고리를 선택해 주세요")
            return
        }
        if appointed_at == ""{
            OKDialog("날짜를 선택해 주세요")
            return
        }
        if placeField.text == ""{
            OKDialog("모이는 장소를 적어주세요")
            return
        }
        if introduceLabel.text == placeHolder{
            OKDialog("클럽을 소개해 주세요")
            return
        }
        
        if introduceLabel.text!.count < 40{
            OKDialog("어떤 클럽인지 조금 더 듣고 싶어요😚\n(최소 40자 이상 입력)")
            return
        }
        
        if GENDER == 0{
            gender_fix = "none"
        }
        else if GENDER == 1{
            gender_fix = "F"
        } else if GENDER == 2{
            gender_fix = "M"
        }
        
        let alert = UIAlertController(title: "", message: "약속을 위해 보증금 1500원을 걷어요.\n목표인원 미달시에도 2인 이상 모이면 약속이 확정돼요.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "취소", style: .cancel)
        let success = UIAlertAction(title: "만들래요!", style: .default, handler: {
            _ in
            
            // 참가 알고리즘 실행
            self.addAction()
        })
        
        alert.addAction(cancel)
        alert.addAction(success)
        self.present(alert, animated: true, completion: nil)
        
        
    }

    
    func addAction(){
        
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let univ: String = selfuser[0].univ
        let campus_type: String = String(selfuser[0].campus_type)
        let room_name: String = titleField.text!
        let category: String = String(selectedCategory)
        let explain: String = introduceLabel.text
        let max_people: String = String(MEMBER)
        let appoint: String = appointed_at
        let gf: String = gender_fix
        let location: String = placeField.text!
        let luu = selfuser[0].uuid
        let nick = selfuser[0].nick_name
        let lgender = selfuser[0].gender
        let mem = selfuser[0]._id
        

        let body: [String:String] = [
            "univ": univ,
            "campus_type": campus_type,
            "room_name": room_name,
             "category": category,
            "activity_explain": explain,
             "how_hot": String(1),
             "max_people": max_people,
             "appointed_at": appoint,
             "gender_fix": gf,
            "location": location,
            "leader_uuid": luu,
            "leader_nick_name": nick,
             "leader_os":"iOS",
            "leader_push_token": "pushtoken",
            "leader_gender": lgender,
            "members": mem//test

         ]

        
//        let url = URL(string: dbInfo.service_url + "api/room/makeRoom")
        let url = dbInfo.service_url + "api/room/makeRoom"
        /****/
        
        Methods.shared.AlamoMultipartUpload(url: URL(string: url)!, params: body, imageToUpload: selectedImage!){ (json) in
            
            let result = json["result"].intValue
            switch result{
            case 101:
                sessionInfo.isRequiredToRefreshHome = true
                sessionInfo.isRequiredToRefreshParti = true
                self.goBack(self)
                break
            case 104:
                self.OKDialog("에러발생")
                break
            case 105:
                self.OKDialog("클럽 가입을 위해서는 1500원의 보증금이 필요해요.\n참여 전 보증금을 충전해주세요!")
                break
                
            case 106:
                self.OKDialog("성별 인증 후에 생성 가능합니다")
                break
                
            case 107:
                self.OKDialog("인증한 성별과 다른 성별을 선택할 수 없습니다.")
                break
            case 206:
                self.OKDialog("계정이 정지되었습니다.\n관리자에게 문의바랍니다.")
                // 계정 정지된 것 DB에 업데이트
                let realm = try! Realm()
                let selfuser = realm.objects(SelfUser.self)
                
                let email = selfuser[0].email

                // 부분적으로 업데이트 하기 (email키값을 찾는다)
                try! realm.write {
                    realm.create(SelfUser.self, value: ["email": email, "is_blocked": true], update: .modified)
               
                }
                break
            default:
                print(result)
                print("error, check log")
                break
            }
            
        }
            
    }
    
    //watcher
    
    //room name & place limit < 25
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
        
            //if(count <= 25){
                //titleCountLabel.text = String(count)+"/25"
                
            //}
        
            return count <= 25
    }
    
    // explain placeholder
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
        
    }
    
    // TextView Place Holder
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
        
    }
    
    // watchers
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //limit
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        
        
        // \n 엔터키 3번이상 못누르게 하기
        if numberOfChars > 4{ if newText[numberOfChars - 1] == "\n" && newText[numberOfChars - 2] == "\n" && newText[numberOfChars - 3] == "\n"{ return false }}
        
        return numberOfChars <= 300    // 300 Limit Value
        
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        
        //watcher
        introduceCountLabel.text = String(textView.text.count)+"/300"
        
    }
    
    //Rest
    
    //image picker & crop
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Error: \(info)")
            return
        }
        dismiss(animated: true, completion: nil)
        self.presentCropViewController(image: selectedImage)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {

            let cgimage = image.cgImage!
            let contextImage: UIImage = UIImage(cgImage: cgimage)
            let contextSize: CGSize = contextImage.size
            var posX: CGFloat = 0.0
            var posY: CGFloat = 0.0
            var cgwidth: CGFloat = CGFloat(width)
            var cgheight: CGFloat = CGFloat(height)

            // See what size is longer and create the center off of that
            if contextSize.width > contextSize.height {
                posX = ((contextSize.width - contextSize.height) / 2)
                posY = 0
                cgwidth = contextSize.height
                cgheight = contextSize.height
            } else {
                posX = 0
                posY = ((contextSize.height - contextSize.width) / 2)
                cgwidth = contextSize.width
                cgheight = contextSize.width
            }

            let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)

            // Create bitmap image from context using the rect
            let imageRef: CGImage = cgimage.cropping(to: rect)!

            // Create a new image based on the imageRef and rotate back to the original orientation
            let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

            return image
    }
    
    //crop
    
    func presentCropViewController(image:UIImage) {
      
      let cropViewController = CropViewController(image: image)
      cropViewController.delegate = self
      cropViewController.aspectRatioPreset = .presetSquare
      cropViewController.aspectRatioPickerButtonHidden = true
      cropViewController.aspectRatioLockEnabled = true
        
      present(cropViewController, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
            let size = image.size

            let widthRatio  = targetSize.width  / image.size.width
            let heightRatio = targetSize.height / image.size.height

            // Figure out what our orientation is, and use that to form the rectangle
            var newSize: CGSize
            if(widthRatio > heightRatio) {
                newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
            } else {
                newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
            }

            // This is the rect that we've calculated out and this is what is actually used below
            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

            // Actually do the resizing to the rect using the ImageContext stuff
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return newImage!
        }
    


    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image

        // do your thing...
        let img = resizeImage(image: image, targetSize: CGSize(width: 500, height: 500))
        self.imageView.image = img
        self.selectedImage = img.jpegData(compressionQuality: 0.7)
        
        self.imageBtnView.isHidden = true

        
        //firebase올리고 php서버에 URL저장
        //self.uploadPicToFirebase()

        //self.ppView.image = selectedImage

        //Now use image to create into NSData format
        //let imageData:NSData = image.pngData()! as NSData
        /*back
         let image = UIImage(data:imageData)
         */

        //let base64 = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))

        //let preferences = UserDefaults.standard
        //preferences.set(base64, forKey: "pp")
        //preferences.synchronize()

        dismiss(animated: true, completion: nil)

    }
    
    // opengallery
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
}

extension AddRoomViewController: DateTimePickerDelegate{
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        dateLabel.setTitle(picker.selectedDateString, for: .normal)
    }
    
}
