//
//  IntroduceController.swift
//  eatTogether2
//
//  Created by 최준혁 on 2021/01/24.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class IntroduceController:UIViewController{
    
    @IBOutlet weak var nick_name: UILabel!
    
    @IBOutlet weak var introduce: UITextView!
    
    var introduceMessage = ""
    var nick_name_text = ""
    
    @IBOutlet weak var bottomTabView: UIView!
    @IBAction func backButtonClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        debugLabels()
        
    }    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // tab bar 감추기
        tabBarController?.tabBar.isHidden = true
        //UIView.transition(with: tabBarController!.view, duration: 0.2, options: .transitionCrossDissolve, animations: nil)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    func debugLabels(){
        nick_name.textColor = UIColor(displayP3Red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)
        introduce.textColor = UIColor(displayP3Red: 20/255, green: 20/255, blue: 20/255, alpha: 1.0)
        bottomTabView.backgroundColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        nick_name.text = nick_name_text
        
        //introduce.text = introduceMessage
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSAttributedString.Key.paragraphStyle: style]
        
        
        // 폰트 및 행간격 조절
        nick_name.font = UIFont(name: "NotoSansCJKkr-Light", size: 25)
        introduce.attributedText = NSAttributedString(string: introduceMessage, attributes: attributes)
        introduce.font = UIFont(name: "NotoSansCJKkr-Light", size: 18)
    }
    
}
