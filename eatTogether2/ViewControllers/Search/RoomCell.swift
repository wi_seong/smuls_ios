//
//  RoomCell.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/08/27.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class RoomCell:UICollectionViewCell{
    
    @IBOutlet weak var background_first: UIView!
    
    @IBOutlet weak var background_second: UIView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var member: UILabel!
}
