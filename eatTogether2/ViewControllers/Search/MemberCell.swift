//
//  MemberCell.swift
//  eatTogether2
//
//  Created by 최준혁 on 2021/01/16.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class MemberCell:UICollectionViewCell{
    
    @IBOutlet weak var nick_name: UILabel!
    
    @IBOutlet weak var layout: UIView!
    
}
