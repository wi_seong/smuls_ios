//
//  ReusableController.swift
//  eatTogether2
//
//  Created by 최준혁 on 2021/01/16.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit

class ReusableController:UICollectionReusableView{
    
    @IBOutlet weak var room_title: UILabel!
    
    @IBOutlet weak var backClicked: UIButton!
    
    @IBOutlet weak var attendLayout: UIButton!
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var information1: UILabel!
    
    @IBOutlet weak var information2: UILabel!
    
    @IBOutlet weak var explanation: UITextView!
    
    @IBOutlet weak var member_count: UILabel!
    
    @IBOutlet weak var gender_fix: UILabel!
    
}
