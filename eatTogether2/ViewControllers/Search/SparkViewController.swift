//
//  SparkViewController.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/08/28.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

class SparkViewController:UIViewController{
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
