//
//  leaderCell.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/25.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit


class leaderTableViewCell: UITableViewCell{
    
    @IBOutlet weak var leaderNickName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        leaderNickName.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    public func configureCell(_ nickName: String){
        leaderNickName.text = nickName
    }
}
