//
//  AddViewController.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/08/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import CropViewController

class tempAdd: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate, UIImagePickerControllerDelegate, CropViewControllerDelegate, UINavigationControllerDelegate{
    
    //crop image
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {

            let cgimage = image.cgImage!
            let contextImage: UIImage = UIImage(cgImage: cgimage)
            let contextSize: CGSize = contextImage.size
            var posX: CGFloat = 0.0
            var posY: CGFloat = 0.0
            var cgwidth: CGFloat = CGFloat(width)
            var cgheight: CGFloat = CGFloat(height)

            // See what size is longer and create the center off of that
            if contextSize.width > contextSize.height {
                posX = ((contextSize.width - contextSize.height) / 2)
                posY = 0
                cgwidth = contextSize.height
                cgheight = contextSize.height
            } else {
                posX = 0
                posY = ((contextSize.height - contextSize.width) / 2)
                cgwidth = contextSize.width
                cgheight = contextSize.width
            }

            let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)

            // Create bitmap image from context using the rect
            let imageRef: CGImage = cgimage.cropping(to: rect)!

            // Create a new image based on the imageRef and rotate back to the original orientation
            let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

            return image
        }
    
    //extension
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard var selectedImage = info[.originalImage] as? UIImage else {
            print("Error: \(info)")
            return
        }
        dismiss(animated: true, completion: nil)
        self.presentCropViewController(image: selectedImage)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    //crop
    
    func presentCropViewController(image:UIImage) {
      
      let cropViewController = CropViewController(image: image)
      cropViewController.delegate = self
      cropViewController.aspectRatioPreset = .presetSquare
      cropViewController.aspectRatioPickerButtonHidden = true
      cropViewController.aspectRatioLockEnabled = true
        
      present(cropViewController, animated: true, completion: nil)
    }

    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image

        // do your thing...
        self.display.image = image
        self.selectedImage = image.jpegData(compressionQuality: 1)

        //firebase올리고 php서버에 URL저장
        //self.uploadPicToFirebase()

        //self.ppView.image = selectedImage

        //Now use image to create into NSData format
        let imageData:NSData = image.pngData()! as NSData
        /*back
         let image = UIImage(data:imageData)
         */

        let base64 = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))

        let preferences = UserDefaults.standard
        preferences.set(base64, forKey: "pp")
        preferences.synchronize()

        dismiss(animated: true, completion: nil)

    }
    
    //add room
    @IBAction func addRoomClicked(_ sender: Any) {
        
        if(selectedImage != nil){
            addAction()
        }else{
            print("No image selected")
        }
        
    }
    
    func addAction(){
        
        let roomName = "test"
        let date = "0000.00.00"
        let gender = "M"
        let category = 0
        let explain = "test_explain"
        let member = 1
        let reported_count = 0
        let is_blocked = "false"
    
        let email = "test@gmail.com"

        let body: [String:String] = [
             "univ": "sejong",
             "campus_type":"0",
             "room_id": String(Int.random(in: 0..<15008)),
             "room_name":roomName,
             "category":String(0),
             "activity_explain":"explain",
             //"food_name":"food",
             "how_hot":String(1),
             "max_people": String(member),
             "appointed_at": String(date),
             "gender_fix": gender,
             "leader_uuid":"test",
             "leader_nick_name":"test",
             "leader_os":"ios",
             "leader_push_token":"test",
             "leader_gender":"M",
             "members":"5fe45b7de109e15334244146"//test
         ]
        
//        let url = URL(string: dbInfo.service_url + "api/room/makeRoom")
        let url = dbInfo.service_url + "api/room/makeRoom"
        /****/
        
        Methods.shared.AlamoMultipartUpload(url: URL(string: url)!, params: body, imageToUpload: selectedImage!){ (json) in
            
            let result = json["result"].intValue
            switch result{
            case 101:
                //sessionInfo.attending = true
                self.backClicked(self)
            default:
                print(result)
                print("error, check log")
            }
            
        }
        
        /****/
        
        /*
        Methods.shared.AlamoPost( url: url, body: body){ (json) in
            
            let result = json["result"].intValue
            switch result{
            case 101:
                sessionInfo.attending = true
                self.backClicked(self)
            default:
                print(result)
                print("error, check log")
            }
        }
         */
            
    }
    
    //back
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //add image
    var imagePicker = UIImagePickerController()
    var selectedImage:Data? = nil
    
    @IBOutlet weak var display: UIImageView!
    
    @IBAction func displayTapped(_ sender: Any) {
        
        //let tappedImage = sender.view as! UIImageView
        
        let alert = UIAlertController(title: "사진 선택", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "갤러리", style: .default, handler: { _ in
            
            self.openGallary()
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "카메라", style: .default, handler: { _ in
            self.openCamera()
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "이미지삭제", style: .default, handler: { _ in
            //이미지삭제
            self.display.image = nil
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "취소", style: .cancel, handler: { _ in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "notice", message: "You don't have any camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
   
        
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //room name
    @IBOutlet weak var roomNameInput: UITextField!
    
    //room name limit < 15
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 15
        }
    
    //same gender
    var SAME_GENDER = false
    
    @IBOutlet weak var sameGenderLayout: UIButton!
    
    @IBAction func sameGenderClicked(_ sender: Any) {
        
        if(!SAME_GENDER){
            //Set green
            sameGenderLayout.setTitleColor(
                UIColor(red: 97/255, green: 219/255, blue: 213/255, alpha: 1.0), for: .normal)
        }else{
            //Set default
            sameGenderLayout.setTitleColor(UIColor.darkGray, for: .normal)
        }
        
        SAME_GENDER = !SAME_GENDER
        
    }
    
    //category
    @IBOutlet weak var categoryButtonLayout: UIButton!
    @IBAction func categoryButtonClicked(_ sender: Any) {
        if(!dateButtonLayout.isHidden){
            dateView.isHidden = true
        }
        pickerView.isHidden = false
    }
    
    //category - pickerview
    
    var categoryData: [String] = ["  식사·맛집·카페  ", "  스터디·책·수업  ", "  전시·공연·게임·운동  ", "  기타  "]
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return categoryData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return categoryData[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int) {
        
        categoryButtonLayout.setTitle(categoryData[row], for: .normal)
        print(categoryData[row])
        
        self.pickerView.isHidden = true
        
    }
    
    //date
    @IBOutlet weak var dateButtonLayout: UIButton!
    
    @IBAction func dateButtonClicked(_ sender: Any) {
        
        if(!pickerView.isHidden){
            pickerView.isHidden = true
        }
        dateView.isHidden = false
        
    }
    
    @objc func changed(){
        
        dateView.isHidden = true
        
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .short
        dateformatter.timeStyle = .short
        
        let date = dateformatter.string(from: dateView.date)
        dateButtonLayout.setTitle("  "+date+"  ", for: .normal)
    }
    
    //date - dateView
    @IBOutlet weak var dateView: UIDatePicker!
    
    //member count & layout
    var MEMBER = 0
    
    @IBOutlet weak var memberCountLayout: UIButton!
    
    @IBAction func memberMinusClicked(_ sender: Any) {
        
        if(MEMBER > 1){
            MEMBER = MEMBER - 1
            memberCountLayout.setTitle(String(MEMBER), for: .normal)
        }
        
    }
    
    @IBAction func memberPlusClicked(_ sender: Any) {
        
        MEMBER = MEMBER + 1
        memberCountLayout.setTitle(String(MEMBER), for: .normal)
        
    }
    
    //explain
    @IBOutlet weak var explainInput: UITextView!
    @IBOutlet weak var textWatcher: UILabel!
    
    //explain - limit
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //hide keyboard when touch return
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        
        //limit
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        
        
        // \n 엔터키 3번이상 못누르게 하기
        if numberOfChars > 4{ if newText[numberOfChars - 1] == "\n" && newText[numberOfChars - 2] == "\n" && newText[numberOfChars - 3] == "\n"{ return false }}
        
        return numberOfChars <= 300    // 300 Limit Value
        
    }
    
    //explain - watcher
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        
        //watcher
        textWatcher.text = String(textView.text.count)+"/300"
        
    }
    
    //explain - placeholder
    // TextView Place Holder
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
        
    }
    
    // TextView Place Holder
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "방을 자유롭게 소개해주세요!\n\n모임 취지, 만날 장소,\n개인이 지참해야할 예상금액 등"
            textView.textColor = UIColor.lightGray
        }
    }
    
    //keyboard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      unregisterForKeyboardNotifications()
    }

    func registerForKeyboardNotifications() {
        // 옵저버 등록
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil
        )
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil
        )
    }
    
    func unregisterForKeyboardNotifications() {
        // 옵저버 등록 해제
        NotificationCenter.default.removeObserver(self,     name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
      if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
        
        let keybaordRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keybaordRectangle.height
        
        self.view.frame.origin.y -= keyboardHeight
        
      }
        
    }
      
    @objc private func keyboardWillHide(_ notification: Notification) {
        
      if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
        
        let keybaordRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keybaordRectangle.height
        
        self.view.frame.origin.y = 0
      }
        
    }
    
    //main
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //layout
        categoryButtonLayout.layer.cornerRadius = 8
        
        //dateButtonLayout.layer.cornerRadius = 8
        //dateView.backgroundColor = UIColor.gray
        
        explainInput.layer.cornerRadius = 8
        
        memberCountLayout.layer.cornerRadius = 8
        
        //delegate
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        roomNameInput.delegate = self
        
        explainInput.delegate = self
        
        //attach changed listener to dateView
        dateView.addTarget(self, action: #selector(changed), for: .valueChanged)
        
        //explain placeholder setting
        explainInput.text = "방을 자유롭게 소개해주세요!\n\n모임 취지, 만날 장소,\n개인이 지참해야할 예상금액 등"
        explainInput.textColor = UIColor.lightGray
        
        //dateView restrict past
        dateView.minimumDate = Date()
        
    }
    
}
