//
//  RoomDetailController.swift
//  eatTogether2
//
//  Created by 최준혁 on 2020/09/25.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
/*
class temp2:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var clicked_nickname = ""
    var clicekd_introudce = ""
    var MemberList = [MemberInfo]()
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemberCell", for: indexPath) as! MemberCell
        
        cell.nick_name.text = MemberList[indexPath.row].nick_name
        clicked_nickname = MemberList[indexPath.row].nick_name
        clicekd_introudce = MemberList[indexPath.row].introduce
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.goIntroduce))
        cell.layout.addGestureRecognizer(gesture)
        
        /*
        // go RoomDetail
         let storyboard = UIStoryboard (name: "Home", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "RoomDetailController")as! RoomDetailController

         // Communicate with new VC - These values are stored in the destination
         // you can set any value stored in the destination VC here
         vc.room_id = RoomList[indexPath!.row].room_id
         vc.img = RoomList[indexPath!.row].img
         vc.room_name = RoomList[indexPath!.row].room_name
         vc.activity_explain = RoomList[indexPath!.row].activity_explain
         vc.appointed_at = RoomList[indexPath!.row].appointed_at
         vc.current_people = RoomList[indexPath!.row].current_people
         vc.gender_fix = RoomList[indexPath!.row].gender_fix
         vc.leader = RoomList[indexPath!.row].leader
         vc.max_people = RoomList[indexPath!.row].max_people
         vc.members = RoomList[indexPath!.row].members
         vc.location = RoomList[indexPath!.row].location
         
         self.navigationController?.pushViewController(vc, animated: true)
         */
        
        return cell
    }
    
    @objc func goIntroduce(_ sender:UITapGestureRecognizer){
        
       // go RoomDetail
        let storyboard = UIStoryboard (name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IntroduceController")as! IntroduceController

        // Communicate with new VC - These values are stored in the destination
        // you can set any value stored in the destination VC here
        vc.nick_name_text = clicked_nickname
        vc.introduceMessage = clicekd_introudce
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MemberList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ReusableController", for: indexPath) as! ReusableController
            
            headerView.room_title.text = room_name
            headerView.explanation.text = activity_explain
            headerView.information1.text = appointed_at
            headerView.information2.text = location
            headerView.member_count.text = current_people + "/" + max_people + "명"
            
            
            headerView.gender_fix.text = "· " + gender_fix
            
            /*explanation padding*/
            headerView.explanation.textContainerInset = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0);
            adjustUITextViewHeight(arg: headerView.explanation)
            
            //attend layout & onClick
            
            
            headerView.attendLayout.layer.borderWidth = 1
            
            headerView.attendLayout.layer.borderColor = UIColor.black.cgColor
            headerView.attendLayout.layer.cornerRadius = 25
            headerView.attendLayout.layer.shadowColor = UIColor.black.cgColor
            headerView.attendLayout.layer.masksToBounds = false
            headerView.attendLayout.layer.shadowOffset = CGSize(width: 1, height: 4)
            headerView.attendLayout.layer.shadowRadius = 8
            headerView.attendLayout.layer.shadowOpacity = 0.3
            
            headerView.attendLayout.addTarget(self, action: #selector(attend), for: .touchUpInside)
            
            //back layout & onClick
            //headerView.backClicked.addTarget(self, action: #selector(back), for: .touchUpInside)
            
            //Get Thumbnail
            if (img != nil){
                headerView.thumbnail.image = img!
            }
            
            //return
            return headerView
            
        default: assert(false, "Nope")
            
        }
        
    }
    
    @objc func attend(_ sender: UIButton) {
        // your code goes here
        
    }
    
    @objc func back(_ sender: UIButton) {
        // your code goes here
        _ = navigationController?.popViewController(animated: true)
    }
    
    var img: UIImage?
    var room_id = ""
    var room_name = ""
    var activity_explain = ""
    var appointed_at = ""
    var campus_type = ""
    var created_at = ""
    var current_people = ""
    var gender_fix = ""
    var how_hot = ""
    var location = ""
    var leader:NSDictionary = [:]
    var max_people = ""
    var members:Array<Any> = []
    var reported_count = ""
    var updated_at = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        debugLabels()
        
    }
    
    func debugLabels(){
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backClicked), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
        self.title = "클럽 상세보기"
        
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
        setMembers()
    }
    
    func setMembers(){
        
        //Repeat leader + member count
        for i in 0 ..< (members.count)
        {
            
            if(i == 0){
                self.MemberList.append(MemberInfo(leader))
            }else{
                self.MemberList.append(MemberInfo(members[i]))
            }
            
        }
        
    }
    
    @objc func backClicked(){
        navigationController?.popViewController(animated: true)
        
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        
        if(arg.text.count > 130){
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        }
        
    }
    
    func participating(){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let uuid = selfuser[0].uuid
        let id = room_id
        
        let url = dbInfo.service_url + "api/requestParticipatingInRoom"
        
        let body = ["room_id": id,
                    "uuid": uuid
        ]
            
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in
            
            let result = json["result"].intValue
            
            print(uuid)
            print(result)
            
            switch result{
            case 101:
                self.OKDialog("Joined!\n본 클럽에 가입되셨습니다.")
                break
            case 102:
                self.OKDialog("잘못된 접근입니다. err 102")
                break
                
            case 103:
                self.OKDialog("이미 가입된 클럽입니다.\n참여중인 방 목록을 확인해주세요.")
                break
                
            case 104:
                self.OKDialog("모집이 마감되었습니다.")
                break
                
            case 105:
                self.OKDialog("모집이 마감되었습니다.")
                break
                
            case 106:
                self.OKDialog("성별 인증 후에 가입 가능합니다")
                break
                
            case 107:
                self.OKDialog("참여할 수 없는 클럽입니다.")
                break
                
            case 108:
                self.OKDialog("클럽 가입을 위해서는 1500원의 보증금이 필요해요.\n참여 전 보증금을 충전해주세요!")
                break
                
            default:
                print(result)
                break
            }
        }
    }
    
    // Button actions
    
    @IBAction func participatingAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "보증금이 1,500원 차감됩니다. ", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "취소", style: .cancel)
        let success = UIAlertAction(title: "확인했어요", style: .default, handler: {
            _ in
            
            // 참가 알고리즘 실행
            self.participating()
        })
        
        alert.addAction(cancel)
        alert.addAction(success)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
}
*/

