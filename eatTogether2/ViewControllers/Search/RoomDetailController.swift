//
//  RoomDetailController.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/25.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class RoomDetailController: UIViewController {

    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var roomTitleLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var timeChar: UILabel!
    
    @IBOutlet weak var locationChar: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var explainTextView: UITextView!
    
    @IBOutlet weak var joinButtonLabel: UIButton!
    
    @IBOutlet weak var memberCountLabel: UILabel!
    
    @IBOutlet weak var genderFixLabel: UILabel!
    
    @IBOutlet weak var joinBtnView: UIView!
    
    @IBOutlet weak var memberChar: UILabel!
    
    var roomInfo: RoomInfo?
    var Parti: Participating?
    
    var MemberList = [MemberInfo]()
    //var leader:NSDictionary = [:]
    //var members:Array<Any> = []
    var leader:JSON = [:]
    var members:JSON = []
    
    var flag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        debugLabels()
        putDataInViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // tab bar 보이기(애니메이션)
        tabBarController?.tabBar.isHidden = false
        UIView.transition(with: tabBarController!.view, duration: 0.2, options: .transitionCrossDissolve, animations: nil)
    }
    
    
    private func debugLabels(){
        //self.title = "클럽 상세보기"
        
        timeChar.backgroundColor = UIColor(displayP3Red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        locationChar.backgroundColor = UIColor(displayP3Red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        memberChar.backgroundColor = UIColor(displayP3Red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        
        joinButtonLabel.backgroundColor = UIColor(displayP3Red: 237/255, green: 248/255, blue: 232/255, alpha: 1.0)
        joinButtonLabel.layer.borderWidth = 1
        joinButtonLabel.layer.borderColor = UIColor.black.cgColor
        joinButtonLabel.layer.cornerRadius = 25
        joinButtonLabel.layer.shadowColor = UIColor.black.cgColor
        joinButtonLabel.layer.masksToBounds = false
        joinButtonLabel.layer.shadowOffset = CGSize(width: 1, height: 3)
        joinButtonLabel.layer.shadowRadius = 6
        joinButtonLabel.layer.shadowOpacity = 0.2
        
        
        // 폰트 셋팅
        roomTitleLabel.font = UIFont(name: "NotoSansCJKkr-Regular", size: 21)
        timeChar.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
        timeLabel.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
        locationChar.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
        locationLabel.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
        
        explainTextView.font = UIFont(name: "NotoSansCJKkr-Regular", size: 16)
        
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        view.backgroundColor = UIColor(displayP3Red: 254/255, green: 255/255, blue: 254/255, alpha: 1.0)
        
        if flag == 1{
            // 조인 버튼 없애기
            joinButtonLabel.isHidden = true
        }
        
        let lbutton = UIButton(type: UIButton.ButtonType.custom)
        lbutton.setImage(UIImage(named: "room_detail_asset_back"), for: .normal)
        lbutton.addTarget(self, action:#selector(backButton), for: .touchUpInside)
        lbutton.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        let lbarButton = UIBarButtonItem(customView: lbutton)
        self.navigationItem.leftBarButtonItem = lbarButton
        
        
        
        
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func putDataInViews(){
        
        if flag == 0{ // 홈화면에서 왔을 때
        
        
            roomTitleLabel.text = roomInfo!.room_name
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 5
            let attributes = [NSAttributedString.Key.paragraphStyle: style]
            
            explainTextView.attributedText = NSAttributedString(string: roomInfo!.activity_explain, attributes: attributes)
            explainTextView.font = .systemFont(ofSize: 16)
            //explainTextView.text = roomInfo!.activity_explain
            timeLabel.text = roomInfo!.appointed_at
            locationLabel.text = roomInfo!.location
            memberCountLabel.text = String(roomInfo!.current_people) + "/" + String(roomInfo!.max_people) + "명"
//            genderFixLabel.text = "· " + roomInfo!.gender_fix
            imageView.image = roomInfo!.img
            //adjustUITextViewHeight(arg: explainTextView)
            
            for i in 0 ..< members.count{
                self.MemberList.append(MemberInfo(members[i]))
            }
        }
        else if flag == 1{ // 참여중에서 왔을 때
            roomTitleLabel.text = Parti!.room_name
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 5
            let attributes = [NSAttributedString.Key.paragraphStyle: style]
            
            explainTextView.attributedText = NSAttributedString(string: Parti!.activity_explain, attributes: attributes)
            explainTextView.font = .systemFont(ofSize: 16)
            //explainTextView.text = Parti!.activity_explain
            timeLabel.text = Parti!.koreanTime
            locationLabel.text = Parti!.location
            memberCountLabel.text = Parti!.current_people + "/" + Parti!.max_people + "명"
            genderFixLabel.text = "· " + Parti!.gender_fix
            imageView.image = Parti!.img
            //adjustUITextViewHeight(arg: explainTextView)
            
            for i in 0 ..< members.count{
                self.MemberList.append(MemberInfo(members[i]))
            }
        }
    }
    
    private func adjustUITextViewHeight(arg : UITextView)
    {
        //arg.translatesAutoresizingMaskIntoConstraints = true
        //arg.sizeToFit()
        // textView밑에 여백주기
        //textViewHeight.constant = arg.contentSize.height + 15
        
    }
    
    
    
    // Button clicked
    
    
    @IBAction func joinButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "약속을 위해 보증금 1500원을 걷어요.\n약속시간과 장소 확인 하셨죠? ", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "취소", style: .cancel)
        let success = UIAlertAction(title: "참여할래요", style: .default, handler: {
            _ in
            
            // 참가 알고리즘 실행
            self.participating()
        })
        
        alert.addAction(cancel)
        alert.addAction(success)
        self.present(alert, animated: true, completion: nil)
        
    }
    

    func participating(){
        
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let uuid = selfuser[0].uuid
        let id = roomInfo!.room_id
        
        let url = dbInfo.service_url + "api/requestParticipatingInRoom"
        
        let body = ["room_id": id,
                    "uuid": uuid
        ]
            
        Methods.shared.ServiceAlamoPost( url: url, body: body){ (json) in
            
            let result = json["result"].intValue
            
            print(uuid)
            print(result)
            
            switch result{
            case 101:
                sessionInfo.isRequiredToRefreshHome = true
                sessionInfo.isRequiredToRefreshParti = true
                
                // 멤버 목록 업데이트 및 참가 인원수 추가
                self.refreshMemberList()
                
                self.OKDialog("Joined!\n본 클럽에 가입되셨습니다.")
                break
            case 102:
                self.OKDialog("잘못된 접근입니다. err 102")
                break
                
            case 103:
                self.OKDialog("이미 가입된 클럽입니다.\n참여중인 방 목록을 확인해주세요.")
                break
                
            case 104:
                self.OKDialog("모집이 마감되었습니다.")
                break
                
            case 105:
                self.OKDialog("모집이 마감되었습니다.")
                break
                
            case 106:
                self.OKDialog("성별 인증 후에 가입 가능합니다")
                break
                
            case 107:
                self.OKDialog("참여할 수 없는 클럽입니다.")
                break
                
            case 108:
                self.OKDialog("클럽 가입을 위해서는 1500원의 보증금이 필요해요.\n참여 전 보증금을 충전해주세요!")
                break
            case 206:
                self.OKDialog("계정이 정지되었습니다.\n관리자에게 문의바랍니다.")
                // 계정 정지된 것 DB에 업데이트
                let realm = try! Realm()
                let selfuser = realm.objects(SelfUser.self)
                
                let email = selfuser[0].email

                // 부분적으로 업데이트 하기 (email키값을 찾는다)
                try! realm.write {
                    realm.create(SelfUser.self, value: ["email": email, "is_blocked": true], update: .modified)
               
                }
                break
            default:
                print(result)
                break
            }
        }
    }
    
    private func refreshMemberList(){
        
        // tableview reload하기
        let realm = try! Realm()
        
        let selfuser = realm.objects(SelfUser.self)
        
        let member: JSON = ["member": ["_id": selfuser[0]._id, "nick_name": selfuser[0].nick_name, "introduce": selfuser[0].introduce, "uuid": selfuser[0].uuid]]
        
        MemberList.append(MemberInfo(member))
        
        tableView.reloadData()
        
        
        // 인원수 추가해주기
        
//        roomInfo!.current_people = String(Int(roomInfo!.current_people)! + 1)
//        memberCountLabel.text = roomInfo!.current_people + "/" + roomInfo!.max_people + "명"
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RoomDetailController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MemberList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(MemberList[indexPath.row].nick_name)
        
        if indexPath.row == 0{
            var cell: leaderTableViewCell
            cell = tableView.dequeueReusableCell(withIdentifier: "leaderCell") as! leaderTableViewCell
            cell.configureCell(MemberList[indexPath.row].nick_name)
            return cell
            
        } else{
            var cell: memberTableViewCell
            cell = tableView.dequeueReusableCell(withIdentifier: "memberCell") as! memberTableViewCell
            cell.configureCell(MemberList[indexPath.row].nick_name)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard (name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IntroduceController")as! IntroduceController

        // Communicate with new VC - These values are stored in the destination
        // you can set any value stored in the destination VC here
        vc.nick_name_text = MemberList[indexPath.row].nick_name
        vc.introduceMessage = MemberList[indexPath.row].introduce
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
