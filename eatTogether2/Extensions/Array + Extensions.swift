//
//  Array + Extensions.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2021/01/23.
//  Copyright © 2021 alomteamd. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {

    mutating func remove(_ element: Element) {
        _ = index(of: element).flatMap {
            self.remove(at: $0)
        }
    }
}

