//
//  UITextField + Extension.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/27.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit


extension UITextField{
       func setLeftPaddingPoints(_ amount:CGFloat){ //왼쪽에 여백 주기
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
     
        func setRightPaddingPoints(_ amount:CGFloat) { //오른쪽에 여백 주기
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }

}
