//
//  String + Extension.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/29.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}
