//
//  UIView + Extension.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/31.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit

// MARK : - Android Toast 기능 추가함 아래 extension
extension UIViewController{
    func Toast(message: String){
        DispatchQueue.main.async {

            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.width/2-75, y: self.view.frame.height - 100, width: 150, height: 40))
            toastLabel.textAlignment = .center
            toastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.black
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 15
            toastLabel.clipsToBounds = true
            toastLabel.text = message
            self.view.addSubview(toastLabel)
            
            UIView.animate(withDuration: 4.0, delay: 0.5, options: .curveEaseOut, animations: {toastLabel.alpha = 0.0}){
                (isCompleted) in
                toastLabel.removeFromSuperview()
            }
        }
    }
    
    func alert(title: String, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        actions.forEach {
            alertController.addAction($0)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}

