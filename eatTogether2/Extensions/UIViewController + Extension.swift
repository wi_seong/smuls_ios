//
//  UIViewController + Extension.swift
//  eatTogether2
//
//  Created by David Lee on 2020/09/22.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit


extension UIViewController{
    
    func OKDialog(_ message: String){
        
        DispatchQueue.main.async {
            
        
            let message = NSLocalizedString(message,
                                            comment: "message")
            let actions = [
                UIAlertAction(title: NSLocalizedString("OK", comment: "Alert ok"),
                              style: .`default`,
                              handler: nil)
            ]
            
            self.alert(title: "", message: message, actions: actions)
        }
    }
    
    // tabbar 애니메이션으로 감추고 보이기
    func setTabBar(hidden:Bool) {
        guard let frame = self.tabBarController?.tabBar.frame else {return }
        if hidden {
            UIView.animate(withDuration: 0.3, animations: {
                self.tabBarController?.tabBar.frame = CGRect(x: frame.origin.x, y: frame.origin.y + frame.height, width: frame.width, height: frame.height)
            })
        }else {

            UIView.animate(withDuration: 0.3, animations: {
                self.tabBarController?.tabBar.frame = UITabBarController().tabBar.frame

            })
        }
    }
    

}
