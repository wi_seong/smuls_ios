//
//  dbAccess.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/19.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation


struct dbInfo{
    
    /*******************************************************/
    // https://smuls.com은 SSL보안 URL임.
    //static let secure_url = "http://15.165.137.161:3000/"
    static let secure_url = "https://smuls.com/"
    
    
    
    /*******************************************************/
    
    
    /*******************************************************/
    //보안서버, 채팅서버 외 나머지 ( 방만들기 등, 그 외 모든 서버)
    //static let service_url = "http://15.165.137.161:3003/"
    static let service_url = "https://smuls.com/"
    
    /*******************************************************/
    
    
    
    /*******************************************************/
    // 채팅 서버
    //static let chatting_url = "http://15.165.137.161:3030"
    static let chatting_url = "http://15.165.137.161:3030"
    static let chat_url = "https://smuls.com/"
    
    /*******************************************************/
}
