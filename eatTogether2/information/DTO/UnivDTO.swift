//
//  UnivDTO.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/11/26.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation
import RealmSwift


class campusDTO: Object{
    @objc dynamic var num: Int = 0
    @objc dynamic var name = ""
    
    override static func primaryKey() -> String? {
        return "num"  //기본 키값이다.
    }
    
}

class UnivDTO: Object{
    
    @objc dynamic var univ_list_en = ""
    @objc dynamic var univ_list_kr = ""
    @objc dynamic var campus_count: Int = 1
    
    override static func primaryKey() -> String? {
        return "univ_list_en"  //기본 키값이다.
    }
 
}
