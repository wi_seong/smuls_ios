//
//  UserDTO.swift
//  eatTogether2
//
//  Created by David Lee on 2020/08/19.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation
import RealmSwift

class SelfUser: Object{
    
    @objc dynamic var _id = ""
    @objc dynamic var uuid = ""
    @objc dynamic var nick_name = ""
    @objc dynamic var email = ""
    @objc dynamic var univ = ""
    @objc dynamic var campus_type: Int = 1
    @objc dynamic var gender = ""
    @objc dynamic var name = ""
    @objc dynamic var naver_id: Int = 0
    @objc dynamic var manner_score = 1.001
    @objc dynamic var introduce = ""
    @objc dynamic var device_id = ""
    @objc dynamic var push_token = ""
    @objc dynamic var is_blocked = false
    @objc dynamic var coin = 0
    @objc dynamic var cur_matching = ""
    
    override static func primaryKey() -> String? {
        return "email"  //email이 기본 키값이다.
    }
    
    
    // 재정의하여 인덱스에 추가 속성을 설정.
    override class func indexedProperties() -> [String] { return ["email", "uuid"] }

    
    
}
