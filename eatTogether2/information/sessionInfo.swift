//
//  sessionInfo.swift
//  eatTogether2
//
//  Created by HYUNSEUNG LEE on 2020/10/03.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import Foundation


struct sessionInfo{
    
    static var device_id = ""
    static var pushToken = ""
    static var chatting_room_id = ""
    static var isRequiredToRefreshHome = false
    static var isRequiredToRefreshParti = false

}
