//
//  Post.swift
//  eatTogether2
//
//  Created by David Lee on 2020/09/22.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Methods: NSObject{
    
    static let shared = Methods()
    
    internal func ServiceAlamoPost(url: String, body: Parameters, handler: @escaping (_ json: JSON) -> Void){
        
        let header: HTTPHeaders = ["Content-Type" : "application/json", "server" : "service"]
        AF.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { response in

            switch response.result{
            case.success:
                guard let data = response.data else {return}
                let json = JSON(data)
                handler(json)
            
            default:
                print("Error")
                let result = 999999
                let json: JSON = ["result": result]
                handler(json)
            }
        }
        
    }
    
    internal func AlamoPost(url: String, body: Parameters, handler: @escaping (_ json: JSON) -> Void){
    
        /**/
        
        let header: HTTPHeaders = ["Content-Type" : "multipart/form-data"]
        AF.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: header).responseJSON { response in

            switch response.result{
            case.success:
                guard let data = response.data else {return}
                let json = JSON(data)
                handler(json)
            
            default:
                print("Error")
                let result = 999999
                let json: JSON = ["result": result]
                handler(json)
            }
        }
        
    }
    
    internal func SecureAlamoPost(url: String, body: Parameters, handler: @escaping (_ json: JSON) -> Void){
        // 보안을 위해 api-key를 설정한다. 이 앱에서만 접근 가능하도록 설정
        let header: HTTPHeaders = ["Content-Type" : "application/json", "api-key" : "96b9445c-2fdf-49b2-8555-147ca47d7fac", "server" : "secure"]
        
        AF.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { response in

            //print (response)
            switch response.result{
            case.success:
                guard let data = response.data else {return}
                let json = JSON(data)
                handler(json)
            
            default:
                print("Alamo Error")
                let result = 999999
                let json: JSON = ["result": result]
                handler(json)
            }
        }
        
    }
    
    internal func AlamoMultipartUpload(url:URL, params: Parameters, imageToUpload : Data, handler: @escaping (_ json: JSON) -> Void)
        {

            let headers: HTTPHeaders = ["Content-Type":"multipart/form-data", "server" : "service"]
        //let headers: HTTPHeaders = ["server" : "service"]

            AF.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                multipartFormData.append(imageToUpload, withName: "img", fileName: "img.jpg", mimeType: "image/jpg")
                
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response{ response in

                switch response.result{
                case.success:
                    guard let data = response.data else {return}
                    let json = JSON(data)
                    handler(json)
                
                default:
                    print("Alamo Error")
                    let result = 999999
                    let json: JSON = ["result": result]
                    handler(json)
                }
                
            }
        }
    
    internal func ChatAlamoPost(url: String, body: Parameters, handler: @escaping (_ json: JSON) -> Void){
        // 보안을 위해 api-key를 설정한다. 이 앱에서만 접근 가능하도록 설정
        let header: HTTPHeaders = ["Content-Type" : "application/json", "server" : "chat"]
        
        AF.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { response in

            //print (response)
            switch response.result{
            case.success:
                guard let data = response.data else {return}
                let json = JSON(data)
                handler(json)
            
            default:
                print("Alamo Error")
                let result = 999999
                let json: JSON = ["result": result]
                handler(json)
            }
        }
        
    }
    
    
}
