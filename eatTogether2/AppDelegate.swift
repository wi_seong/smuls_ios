//
//  AppDelegate.swift
//  eatTogether2
//
//  Created by David Lee on 2020/07/20.
//  Copyright © 2020 alomteamd. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import AudioToolbox
import NaverThirdPartyLogin
import SwiftyStoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        // 알림 delegate 채택
        UNUserNotificationCenter.current().delegate = self
        
        //---------------------------- 다크모드해제 ---------------------------/
        
        if #available(iOS 13.0, *) {
          self.window?.overrideUserInterfaceStyle = .light
        }
        
        
        // Tabbar 라인 없애기
        //UITabBar.appearance().clipsToBounds = false
        UITabBar.appearance().layer.borderWidth = 0
        UITabBar.appearance().backgroundColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        
        // Navigation bar bottom 라인 없애기
        UINavigationBar.appearance().shadowImage = UIImage()
        //UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().backgroundColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 249/255, green: 249/255, blue: 248/255, alpha: 1.0)
        UINavigationBar.appearance().isTranslucent = false
        
        
        //---------------------------- 네이버아이디 로그인 설정 ---------------------------/
        let instance = NaverThirdPartyLoginConnection.getSharedInstance()
        
        // 네이버 앱으로 인증하는 방식을 활성화
        instance?.isNaverAppOauthEnable = true
        
        // 사파리에서 인증하는 방식을 활성화
        instance?.isInAppOauthEnable = true
        
        // 인증 화면을 iPhone 세로 모드에서만 사용하기
        instance?.isOnlyPortraitSupportedInIphone()
        
        // 네이버 아이디로 로그인하기 설정
        // 애플리케이션을 등록할 때 URL Scheme
        instance?.serviceUrlScheme = kServiceAppUrlScheme
        
        // 애플리케이션 등록 후 발급받은 클라이언트 아이디
        instance?.consumerKey = kConsumerKey
        
        // 애플리케이션 등록 후 발급받은 클라이언트 시크릿
        instance?.consumerSecret = kConsumerSecret
        
        // 애플리케이션 이름
        instance?.appName = kServiceAppName
        
        
        //---------------------------- Firebase 설정 ----------------------------/
        FirebaseApp.configure()
        
        
        
        // 알림 요청
        registerForPushNotification()
        
        /*************************************************/
        
        
        // SwiftyStoreKit
        // 서버에 저장되어야하기 때문에 Non-Atomic
        SwiftyStoreKit.completeTransactions(atomically: false) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        
        return true
    }
   
    //토큰 받기
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        let tokenParts = deviceToken.map{data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        sessionInfo.pushToken = token // pushToken 값 넣기
        //sessionInfo.device_id = deviceTokenString
        //print("push토큰: \(token)")
        //SelfUser.push_token = deviceTokenString
        
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        
        //SelfUser.push_token = "none"
        print("APNs registration failed: \(error)")
    }
    
    
    
    //알림 메세지 정보에 관한 것
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        // let aps = userInfo["aps"] as! [String : AnyObject]
    }
    
    // Foreground Notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
          //Handle the notification
          //This will get the text sent in your notification
        
        UIApplication.shared.applicationIconBadgeNumber += 1
        
        let data = notification.request.content.userInfo

        print(data)
        if let type = data[AnyHashable("type")] as? Int {
            print("type: \(type)")
            
            switch type {
            case 1:
                if let room_id = data[AnyHashable("room_id")]! as? String {
                    if sessionInfo.chatting_room_id == room_id {return} // 사용자가 현재 해당 room 채팅방 안에 있는데 메세지가 오는 경우!
                    // DO STUFF, in myValue you will find your custom data
                }
                break
            default:
                break
            }
        }
        
        
        
        
        
        completionHandler([.alert,.sound]) // completionHandler 호출을 해주어야 Notification 노출

        
      }
    
    // Background Notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Handle the notification
        
        UIApplication.shared.applicationIconBadgeNumber += 1
        
        
        //print("did receive")
        let body = response.notification.request.content.body
        //print("*****")
        //print(body)
        
        
        completionHandler()

      }

    
    
    /**************************************************************/
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "eatTogether2")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // Naver ID login delegate
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        NaverThirdPartyLoginConnection.getSharedInstance()?.application(app, open: url, options: options)
        return true
      }
    
    func registerForPushNotification(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]){ (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else {return }
            self.getNotificationSettings()
         
        }
    }
    func getNotificationSettings(){
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            
            guard settings.authorizationStatus == .authorized else {return}
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
            
        }
    }

}

